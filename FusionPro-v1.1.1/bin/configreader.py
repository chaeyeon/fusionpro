# -*- coding: utf-8 -*-
import re				# regular expression module
import sys

class reader:
	def __init__(self, file):   #python은 class의 현재 instance를 self로 받는다.

		re_keyval = re.compile("^\s*([^=\s]+)\s*=\s*(.*)$")	#\s는 white space를 의미하고 ()로 구분된 것들은 group으로 나눌 수 있음 '~ = ~' 형식을 찾는 표현식
		
		self.values = {}
		for line in file:
			if line[0:1] == "#":
				continue
			
			m = re_keyval.match(line)
			
			if not m:
				continue
			
			key = m.group(1)

			key = re.sub(r'^PD_', '', key)
			key = re.sub(r'^PS_', '', key)
			key = re.sub(r'^PG_', '', key)
			key = re.sub(r'^PA_', '', key)
			key = re.sub(r'^DB_', '', key)
			
			value = m.group(2)
			
			self.values[key] = value
			
		re_valref = re.compile("\$\(([^)]+)\)")	# path가 다른 path를 참조하는 경우 (user가 따로 설정하지 않는 부분) ex) $(dataset_directory)/defuse
		
		for key, value in self.values.iteritems():	# dictionary method인 iteritems()는 'key'와 'value'의 쌍을 iterator(반복자)로 반환한다
			while re_valref.match(value):
				m = re_valref.match(value)
				other_key = m.group(1)
				try:
					other_value = self.values[other_key]	# ex) $(dataset_directory)/defuse 에서 dataset_directory을 key로하는 value를 찾는다.
				except KeyError:
					print "Error: no value for %s in config file" % other_key
					sys.exit(1)
					
				re_valrepl = re.compile("\$\(" + other_key + "\)")	
				value = re_valrepl.sub(other_value, value)	# string에서 pattern과 일치하는 부분에 대하여 repl로 교체하여 결과 문자열을 반환한다 
															# 즉, value를 other_value로 바꾸어 value값으로 return 한다.
				
			self.values[key] = value						# 해당 key에 해당하는 value로 value를 assign 시킨다.

		"""for key, value in self.values.iteritems():
			print key
			print value
		"""
          
				
	def has_value(self, key):
		return key in self.values
		
	def get_value(self, key):
		try:
			return self.values[key]
		except KeyError:
			print "Error: no value for %s in config file" % other_key
			sys.exit(1)
			
	def get_values(self, key):
		value = get_value(self, key)
		return set(value.split(","))
	
