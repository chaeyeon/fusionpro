use strict; 
use warnings;


my $filter = $ARGV[0];
my $out_dir = $ARGV[1];
my $redundant = $ARGV[2];
my $final = $ARGV[3];

open (fileHandle, $out_dir."/".$redundant."_".$filter.".fasta") || die "Cannot open ".$redundant."_".$filter.".fasta.\n";
open (fileWriter, ">".$out_dir."/".$final."_".$filter.".fasta") || die "Cannot make new file\n";
my $line;
my @seq;
my @fus;
my $temp_seq="";
my $temp_fus="";
my $red_seq = 1;
my $orf_num = 0;
while($line=<fileHandle>){
	chomp $line;
	if($line =~ />/){
		for(my $j=0; $j<@seq; $j++){
			if($seq[$j] eq $temp_seq){
				$red_seq = 1;
				last;
			}
		}
		if($red_seq == 0){
			if($line =~ /frame/){
				$orf_num ++;
				$temp_fus = substr($temp_fus,0,rindex($temp_fus,"f")+1).$orf_num;
				push @seq, $temp_seq;
				push @fus, $temp_fus;
				print fileWriter $temp_fus."\n".$temp_seq;
			}
			else{
				$orf_num ++;
				$temp_fus = substr($temp_fus,0,index($temp_fus," "))."_".$orf_num." ".substr($temp_fus,index($temp_fus," ")+1);

				push @seq, $temp_seq;
				push @fus, $temp_fus;
				print fileWriter $temp_fus."\n".$temp_seq;
			}
		}

		

		$red_seq = 0;
		$orf_num = 0;
		$temp_seq="";
		$temp_fus = $line;
		for(my $j=0; $j<@fus; $j++){
			if($line =~ /frame/){
				if(substr($line,0,rindex($line,"f")+1) eq substr($fus[$j],0,rindex($fus[$j],"f")+1)){
					if($orf_num < int(substr($fus[$j], rindex($fus[$j],"f")+1))){
						$orf_num = int(substr($fus[$j], rindex($fus[$j],"f")+1));
					}
				}
			}
			else{
				if(substr($line,0,index($line," ")) eq substr($fus[$j],0,index($fus[$j],"_"))){
					if($orf_num < int(substr($fus[$j], index($fus[$j],"_")+1, index($fus[$j]," ")-index($fus[$j],"_")-1))){
						$orf_num = int(substr($fus[$j], index($fus[$j],"_")+1, index($fus[$j]," ")-index($fus[$j],"_")-1));
					}
				}
				else{
					$orf_num = 0;
				}
			}
		}
		
	}
	else {
		$temp_seq .= $line."\n";
	}
}
close(fileHandle);
close(fileWriter);