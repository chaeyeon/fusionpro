#================================================================================
#	통계 자료 스타일 포맷을 만들어주는 module 
#================================================================================

#!/usr/bin/perl
use strict;
use warnings;

package style;

sub css {
	my $whole;
	open(FH, $_[0]) || die $!;
	while(<FH>) {
		$whole .= $_;
	}
	close FH;

	return $whole;
}
1;