#!/usr/bin/perl
use strict;
use warnings;

# Retrieve the ensembl chromosome fastas
my $ensembl_genome_version = $ARGV[0];
my $ensembl_version = $ARGV[1];
my $genome_fasta = $ARGV[2];
my $gene_models = $ARGV[3];
my $dataset_dir = $ARGV[4];


sub wget_gunzip{
	my $url =  $_[0];
	my $filename =  $_[1];
	
	my $filename_gz = $filename.".gz";
	
	system("wget -O $filename_gz $url");
	system("gunzip $filename_gz");
}


my @chromosomes = (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,"X","Y","MT");
foreach my $chromosome (@chromosomes){
	my $chromosome_fasta = "dna.chromosome.$chromosome.fa";
	
	#since GRCh38, file name format has been changed
	if($ensembl_genome_version eq "GRCh38"){
		wget_gunzip("ftp://ftp.ensembl.org/pub/release-$ensembl_version/fasta/homo_sapiens/dna/Homo_sapiens.$ensembl_genome_version.dna.chromosome.$chromosome.fa.gz", "$dataset_dir/$chromosome_fasta");
	}
	else {
		wget_gunzip("ftp://ftp.ensembl.org/pub/release-$ensembl_version/fasta/homo_sapiens/dna/Homo_sapiens.$ensembl_genome_version.$ensembl_version.dna.chromosome.$chromosome.fa.gz", "$dataset_dir/$chromosome_fasta");
	}
}

my $fa_list ="";
foreach my $chromosome (@chromosomes) {
	$fa_list .= "$dataset_dir/dna.chromosome.$chromosome.fa ";
}
system("cat $fa_list > $dataset_dir/Homo_sapiens.$ensembl_genome_version.$ensembl_version.dna.chromosomes.fa");

# Retrieve the ensembl gene models (gtf)
wget_gunzip("ftp://ftp.ensembl.org/pub/release-$ensembl_version/gtf/homo_sapiens/Homo_sapiens.$ensembl_genome_version.$ensembl_version.gtf.gz", $gene_models);


# Retrieve the ensembl cdna fa
if($ensembl_genome_version eq "GRCh38"){
	wget_gunzip("ftp://ftp.ensembl.org/pub/release-$ensembl_version/fasta/homo_sapiens/cdna/Homo_sapiens.$ensembl_genome_version.cdna.all.fa.gz","$dataset_dir/Homo_sapiens.$ensembl_genome_version.$ensembl_version.cdna.all.fa")
}
else{
	wget_gunzip("ftp://ftp.ensembl.org/pub/release-$ensembl_version/fasta/homo_sapiens/cdna/Homo_sapiens.$ensembl_genome_version.$ensembl_version.cdna.all.fa.gz","$dataset_dir/Homo_sapiens.$ensembl_genome_version.$ensembl_version.cdna.all.fa");
}


# Retrieve the ensembl ncrna fa
if($ensembl_genome_version eq "GRCh38"){
	wget_gunzip("ftp://ftp.ensembl.org/pub/release-$ensembl_version/fasta/homo_sapiens/ncrna/Homo_sapiens.$ensembl_genome_version.ncrna.fa.gz","$dataset_dir/Homo_sapiens.$ensembl_genome_version.$ensembl_version.ncrna.fa")
}
else{
	wget_gunzip("ftp://ftp.ensembl.org/pub/release-$ensembl_version/fasta/homo_sapiens/ncrna/Homo_sapiens.$ensembl_genome_version.$ensembl_version.ncrna.fa.gz","$dataset_dir/Homo_sapiens.$ensembl_genome_version.$ensembl_version.ncrna.fa");
}

my $cdna_ncrna_fa = "$dataset_dir/Homo_sapiens.$ensembl_genome_version.$ensembl_version.cdna.all.fa $dataset_dir/Homo_sapiens.$ensembl_genome_version.$ensembl_version.ncrna.fa";

system("cat $cdna_ncrna_fa > $dataset_dir/Homo_sapiens.$ensembl_genome_version.$ensembl_version.cdna.ncrna.chromosomes.fa");
unlink($dataset_dir."/Homo_sapiens.".$ensembl_genome_version.$ensembl_version.".ncrna.fa");


# Retrieve blast database
=top
chdir("./blast");
system("curl -L -u anonymous:guest \"ftp://ftp.ncbi.nlm.nih.gov/blast/db/\" > list.txt");

open (file1, "list.txt") || die "Cannot open list.txt!\n";
my $line = "";
while($line = <file1>){
	$line =~ s/\R|\n//g;
	my $download_file = substr($line,rindex($line," ")+1);
	if(($download_file =~ /^human_genomic/) or ($download_file =~ /^nt/)){
		system("curl -O -u anonymous:guest \"ftp://ftp.ncbi.nlm.nih.gov/blast/db/$download_file\"");
		if($download_file =~ /tar.gz$/){
			system("tar -zxvf $download_file");
			unlink($download_file);
		}
	}
}
unlink("list.txt");
chdir("..");
close(file1);
=cut