use strict;
use warnings;
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);


my ($input, $out) = @ARGV;

gunzip $input => substr($input,0,rindex($input,"."))
        or die "gunzip failed: $GunzipError\n";

open(fileWriter, ">".$out.".fasta") || die "Cannot make new file.\n";
open(fileHandler, substr($input,0,rindex($input,"."))) || die "Cannot open ".$input.".\n";

my $line;
my $turn=1;
while($line=<fileHandler>){
	if($line =~ /^>/){
		my $acc = substr($line,0,-3);
		print fileWriter $acc."\n";
	}
	else{
		print fileWriter $line;
	}

}

close(fileWriter);
close(fileHandler);

