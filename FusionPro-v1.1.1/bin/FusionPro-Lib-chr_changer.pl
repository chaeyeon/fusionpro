use strict;
use warnings;

my $file = $ARGV[0];

open (fileHandle, $file) || die "Cannot open file\n";
open (fileWriter, ">".substr($file,0,rindex($file,"."))."_soapfuse.fa") || die "Cannot make new file\n";
open (fileWriter2, ">".substr($file,0,rindex($file,"."))."_.fa") || die "Cannot make new file\n";

my $line;
my $line2;
while($line=<fileHandle>){
	my @array = split /\t/,$line;
	if($line !~ '#'){
		if($line =~ />/){
		
			my $chr = substr $line,1,index($line," ")-1;
			$line = ">chr".$chr."\n";
			$line2 = ">".$chr."\n";
			print fileWriter $line;
			print fileWriter2 $line2;
		}
		else{
			print fileWriter $line;
			print fileWriter2 $line;
		}
	}
	
}

close(fileHandle);
close(fileWriter);
close(fileWriter2);
