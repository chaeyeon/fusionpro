#===========================================================================#
#		Filtering out false fusion genes and Making final results			#
#		=> Procssing Customized input										#
#===========================================================================#

#!/usr/bin/perl
use strict;
use warnings;
use List::Util qw/ min max /;
use LWP::Simple;
use sequence_extractor_for_custom;



my $arg_string=join(' ',@ARGV);
my @config=split(" ",$arg_string);
my %conf_val;

for (my $i=0;$i<@config;$i++) {
	$conf_val{substr($config[$i],1)} = $config[$i+1];
	$i++;
}

#-bin -filter -cdna -gtf -i -o -r -abun -cutoff -enzyme
my $bin_dir = $conf_val{bin};
my $filter = $conf_val{filter};
my $genome_model = $conf_val{gtf};
my $cdna_fasta = $conf_val{cdna};
my $out_dir = $conf_val{o};
my $ref_dir = $conf_val{r};
my $input= $conf_val{i};
my $fa_dir = "null";
my $abun = "-";
my $cutoff = "null";
my $enzyme = "trypsin";

if($filter !~ /I/){
	$fa_dir = $conf_val{fa};
}
if($filter =~ /X/){
	$abun = $conf_val{abun};
	$cutoff = $conf_val{cutoff};
}
if($conf_val{enzyme}){
	$enzyme = $conf_val{enzyme};
}


#Ensembl gtf based exon_coord generator
#system("perl ".$bin_dir."/FusionPro-Run-coord_generator.pl ".$genome_model." ".$out_dir) if -e $out_dir."/exon_coord.tsv";



#check if gene1 sequence is similar with gene2 sequence
#$_[0] = $gene1
#$_[1] = $gene2
sub Ensembl_paralog_filtration{
	my $gene1 = $_[0];
	my $gene2 = $_[1];
	open (fileHandler, $ref_dir."/Ensembl_paralog_list.tsv") || die "Cannot open Ensembl_paralog_list.tsv\n";
	my $l;
	while($l=<fileHandler>){
		$l =~ s/\R|\n//g;
		if($l =~ /^$gene1[\t]*/){
			if($l =~  /[\t]*$gene2[\t]*/){
				return 1;
			}
		}
		elsif($l =~ /^$gene2[\t]*/){
			if($l =~  /[\t]*$gene1[\t]*/){
				return 1;
			}
		}
	}
	return 0;
}

#$_[0] = $gene1
#$_[1] = $gene2
sub Gencode_pseudogene_filtration{
	my $gene1 = $_[0];
	my $gene2 = $_[1];
	open (fileHandler, $ref_dir."/gencode.v10.pgene.parents_list.txt") || die "Cannot open gencode.v10.pgene.parents_list.txt\n";
	my $l;
	while($l=<fileHandler>){
		$l =~ s/\R|\n//g;
		if($l =~ /$gene1\t$gene2/ or $l =~ /$gene2\t$gene1/){
			return 1;
		}
	}
	return 0;
}

#$_[0] = $gene1
#$_[1] = $gene2
sub Mitelman_annotation{
	my $gene1 = $_[0];
	my $gene2 = $_[1];
	my $anno;
	my $url = "http://cgap.nci.nih.gov/Chromosomes/MCList?op=M&gene=".$gene1."/".$gene2;
	my $content = get $url;
	if(!$content){
		#connection error
		$anno = "error\t."
	}
	
	if($content =~ /$gene1/){
		$anno = "known\thttp://cgap.nci.nih.gov/Chromosomes/MCList?op=M&gene=".$gene1."/".$gene2;
	}
	else{
		$anno = "novel\t.";
	}

	return $anno;
}

sub fusion_analysis{
	my $exon_coord = $genome_model;						#transcripts start, end
	my $gene1 = $_[0];
	my $gene2 = $_[1];
	my $chr1 = $_[2];
	my $chr2 = $_[3];
	my $str1 = $_[4];
	my $str2 = $_[5];
	my $junc1 = $_[6];
	my $junc2 = $_[7];
	my $anno = $_[8];

	my $l;
	my @e1;
	my @e2;
	my @s1;
	my @s2;
	my @seq_pass1;
	my @seq_pass2;
	my @trans1;
	my @trans2;
	my @regst1;
	my @regnd1;
	my @regst2;
	my @regnd2;
	my $break1="";
	my $break2="";
	my $exst1=".";
	my $exnd1=".";
	my $exst2=".";
	my $exnd2=".";
	my @trst1;
	my @trnd1;
	my @trst2;
	my @trnd2;
	my $trst1=0;
	my $trnd1=0;
	my $trst2=0;
	my $trnd2=0;
	my @exst;
	my @exnd;
	my @location1;
	my @location2;
	my $find = 0;

	#check gene1 - if the junction is on exon: $e1=1 / intron: $e1=0
	open (fileHandler2, $exon_coord) || die "Cannot open ".$exon_coord.".\n";
	while($l=<fileHandler2>){
		$l =~ s/\R|\n//g;
		my @arr = split /\t/,$l;
		my $gene = $arr[0];
		my $chr = $arr[1];
		my $exon_junc_len = 0;
		my $intron_junc_len = 0;
		my $e = 0;
		my $trans_id = $arr[4];
		$trans_id =~ s/\R|\n//g;

		if(($gene1 eq $gene) and ($chr1 eq $chr)) {
			$find = 1;
			if($filter =~ /X/){
				my $exp_val=0;
				my $trid = "";
				my $a_l="";
				open(fileHandler3, $abun) || die "Cannot open ".$abun."\n";
				while($a_l=<fileHandler3>){
					$a_l =~ s/\R|\n//g;
					my @a = split /\t/,$a_l;
					$trid = $a[0];
					$exp_val = $a[1];
					if($trid eq $trans_id){
						if($exp_val <= $cutoff){
							last;
						}
					}
				}
				close(fileHandler3);

				if($trid eq $trans_id){
					if($exp_val <= $cutoff){
						next;
					}
				}
			}

			push @exst, split /,/,substr($arr[2],0,length($arr[2])-1);
			push @exnd, split /,/,substr($arr[3],0,length($arr[3])-1);
			@exst= sort {$a <=> $b} @exst;
			@exnd= sort {$a <=> $b} @exnd;

			my $j=0;
			
			$trst1 = int($arr[5]);
			$trnd1 = int($arr[6]);

			push @trst1, $trst1;
			push @trnd1, $trnd1;
		
			my $location ="";
			for ($j=0; $j<@exst; $j++){
				if($exst[$j]<=$junc1){
					if($exnd[$j]>=$junc1){
						$e = 1;
						$location = "exon".($j+1);
						$exst1=$exst[$j];
						$exnd1=$exnd[$j];
						$exon_junc_len += $junc1-$exst[$j]+1;
						last;
					}
					elsif($j == @exst-1){
						$exst1=$exnd[$j]+1;
						$exnd1=$trnd1;
						$intron_junc_len = $junc1-$exnd[$j]+1;
						$location = "intron";
						last;
					}
					else{
						$exon_junc_len += ($exnd[$j]-$exst[$j]+1);
					}
				}
				elsif($e != 1){
					if ($j != 0 and $exnd[$j-1]<$junc1) {
						#$exst and $exnd is for intron start and end coord.
						$exst1=$exnd[$j-1]+1;
						$exnd1=$exst[$j]-1;
						$intron_junc_len = $junc1-$exst1+1;
						$location = "intron";
						last;
					}
					elsif ($j==0) {
						$exst1=$trst1;
						$exnd1=$exst[$j]-1;
						$intron_junc_len = $junc1-$trst1+1; 
						$location = "intron";
						last;
					}
				}

			}

			if(($arr[7]<=$junc1) and ($arr[8]>=$junc1)){
				$e=1;
				$location = "5'utr";
				$exst1=$arr[7];
				$exnd1=$arr[8];
			}
			elsif(($arr[9]<=$junc1) and ($arr[10]>=$junc1)){
				$e=1;
				$location = "3'utr";
				$exst1=$arr[9];
				$exnd1=$arr[10];
			}
			
			if($filter =~ /I/){
				if($location eq "intron"){
					pop @trst1;
					pop @trnd1;
					next;
				}
			}

			push @location1, $location;

			if($exst[0]>$trst1){
				$exon_junc_len += $exst[0]-$trst1+1;
			}
			if($exnd[@exnd-1]<$trnd1){
				$exon_junc_len += $trnd1-$exnd[@exnd-1]+1;
			}

			push @regst1, $exst1;
			push @regnd1, $exnd1;

			
			if($filter !~ /I/){
				my $s1_return;
				if($e){
					$s1_return = sequence_extractor_for_custom::cdna_sequencing($str1, $cdna_fasta, $trans_id, $exon_junc_len, "5", $str1.$str2);
					if (length($s1_return) >= 15 and $s1_return ne "no sequence"){
						push @seq_pass1, 1;
					}
					else{
						push @seq_pass1, 0;
					}
				}
				else{
					my $s1_cdna = sequence_extractor_for_custom::cdna_sequencing($str1, $cdna_fasta, $trans_id, $exon_junc_len, "5", $str1.$str2);
					my $s1_intron = sequence_extractor_for_custom::intron_region_sequencing($fa_dir, $junc1, $str1, $chr1, "5", $intron_junc_len);
					
					if($s1_cdna eq "no sequence"){
						$s1_cdna = "";
					}
					$s1_return = $s1_cdna.$s1_intron;

					if (length($s1_return) >= 15){
						push @seq_pass1, 1;				
					}
					else{
						push @seq_pass1, 0;
					}
				}
				push @trans1, $trans_id;
				push @e1, $e;
				push @s1, $s1_return;
			}
			elsif($e){
				my $s1_return = sequence_extractor_for_custom::cdna_sequencing($str1, $cdna_fasta, $trans_id, $exon_junc_len, "5", $str1.$str2);
				if (length($s1_return) >= 15 and $s1_return ne "no sequence"){
					push @seq_pass1, 1;
				}
				else{
					push @seq_pass1, 0;
				}
				push @trans1, $trans_id;
				push @e1, $e;
				push @s1, $s1_return;
			}
		}
		@exst = ( );
		@exnd = ( );
	}
	close(fileHandler2);

	if($find == 0){
		print "Warning!!! There is no exon coordinates for gene ".$gene1.". Please make sure that a gene name is an official gene symbol.\n";
		return 0;
	}
	
	
	$find = 0;
	#check gene2 - if the junction is on exon: $e2=1 / intron: $e2=0
	open (fileHandler2, $exon_coord) || die "Cannot open ".$exon_coord.".\n";
	while($l=<fileHandler2>){
		$l =~ s/\R|\n//g;
		my @arr = split /\t/,$l;
		my $gene = $arr[0];
		my $chr = $arr[1];
		my $exon_junc_len = 0;
		my $intron_junc_len = 0;
		my $e = 0;
		my $trans_id = $arr[4];
		$trans_id =~ s/\R|\n//g;
		

		if(($gene2 eq $gene) and ($chr2 eq $chr)) {
			$find = 1;
			if($filter =~ /X/){
				my $exp_val=0;
				my $trid = "";
				my $a_l="";
				open(fileHandler3, $abun) || die "Cannot open ".$abun."\n";
				while($a_l=<fileHandler3>){
					$a_l =~ s/\R|\n//g;
					my @a = split /\t/,$a_l;
					$trid = $a[0];
					$exp_val = $a[1];
					if($trid eq $trans_id){
						if($exp_val <= $cutoff){
							last;
						}
					}
				}
				close(fileHandler3);

				if($trid eq $trans_id){
					if($exp_val <= $cutoff){
						next;
					}
				}
			}

			push @exst, split /,/,substr($arr[2],0,length($arr[2])-1);
			push @exnd, split /,/,substr($arr[3],0,length($arr[3])-1);
			@exst= sort {$a <=> $b} @exst;
			@exnd= sort {$a <=> $b} @exnd;

			my $j=0;
			
			$trst2 = int($arr[5]);
			$trnd2 = int($arr[6]);
			
			push @trst2, $trst2;
			push @trnd2, $trnd2;

			my $location="";
			for ($j=0; $j<@exst; $j++){
				if($exst[$j]<=$junc2){
					if($exnd[$j]>=$junc2){
						$e=1;
						$location = "exon".($j+1);
						$exst2=$exst[$j];
						$exnd2=$exnd[$j];
						$exon_junc_len += $junc2-$exst[$j]+1;
						last;
					}
					elsif($j == @exst-1){
						$exst2=$exnd[$j]+1;
						$exnd2=$trnd2;
						$intron_junc_len = $junc2-$exnd[$j]+1;
						$location = "intron";
						last;
					}
					else{
						$exon_junc_len += ($exnd[$j]-$exst[$j]+1);
					}
				}
				elsif($e != 1){
					if ($j != 0 and $exnd[$j-1]<$junc2) {
						#$exst and $exnd is for intron start and end coord.
						$exst2=$exnd[$j-1]+1;
						$exnd2=$exst[$j]-1;
						$intron_junc_len = $junc2-$exst2+1;
						$location = "intron";
						last;
					}
					elsif ($j==0) {
						$exst2=$trst2;
						$exnd2=$exst[$j]-1;
						$intron_junc_len = $junc2-$trst2+1; 
						$location = "intron";
						last;
					}
				}
				
			}

			if(($arr[7]<=$junc2) and ($arr[8]>=$junc2)){
				$e=1;
				$location = "5'utr";
				$exst2=$arr[7];
				$exnd2=$arr[8];
			}
			elsif(($arr[9]<=$junc2) and ($arr[10]>=$junc2)){
				$e=1;
				$location = "3'utr";
				$arr[10] =~ s/\R|\n//g;
				$exst2=$arr[9];
				$exnd2=$arr[10];
			}
			if($filter =~ /I/){
				if($location eq "intron"){
					pop @trst2;
					pop @trnd2;
					next;
				}
			}

			push @location2, $location;

			if($exst[0]>$trst2){
				$exon_junc_len += $exst[0]-$trst2+1;
			}
			if($exnd[@exnd-1]<$trnd2){
				$exon_junc_len += $trnd2-$exnd[@exnd-1]+1;
			}

			push @regst2, $exst2;
			push @regnd2, $exnd2;

			
			if($filter !~ /I/){
				my $s2_return;
				if($e){
					$s2_return = sequence_extractor_for_custom::cdna_sequencing($str2, $cdna_fasta, $trans_id, $exon_junc_len, "3", $str1.$str2);
					if (length($s2_return) >= 15 and $s2_return ne "no sequence"){
						push @seq_pass2, 1;
					}
					else{
						push @seq_pass2, 0;
					}
				}
				else{
					my $s2_intron = sequence_extractor_for_custom::intron_region_sequencing($fa_dir, $junc2, $str2, $chr2, "3", $intron_junc_len);
					my $s2_cdna = sequence_extractor_for_custom::cdna_sequencing($str2, $cdna_fasta, $trans_id, $exon_junc_len, "3", $str1.$str2);
					if($s2_cdna eq "no sequence"){
						$s2_cdna = "";
					}
					$s2_return = $s2_intron.$s2_cdna;
					if (length($s2_return) >= 15){
						push @seq_pass2, 1;				
					}
					else{
						push @seq_pass2, 0;
					}
				}
				push @trans2, $trans_id;
				push @e2, $e;
				push @s2, $s2_return;
			}
			elsif($e){
				my $s2_return = sequence_extractor_for_custom::cdna_sequencing($str2, $cdna_fasta, $trans_id, $exon_junc_len, "3", $str1.$str2);
				if (length($s2_return) >= 15 and $s2_return ne "no sequence"){
					push @seq_pass2, 1;
				}
				else{
					push @seq_pass2, 0;
				}
				push @trans2, $trans_id;
				push @e2, $e;
				push @s2, $s2_return;	
			}
		}
		@exst = ( );
		@exnd = ( );
	}
	close(fileHandler2);
	
	if($find == 0){
		print "Warning!!! There is no exon coordinates for gene ".$gene2.". Please make sure that a gene name is an official gene symbol.\n";
		return 0;
	}


	my $as1=0;
	my $as2=0;
	my $seq;
	$find = 0;
	for($as1=0; $as1<@trans1; $as1++){
		for($as2=0; $as2<@trans2; $as2++){	
			my $region1;
			my $region2;
			my $splicing="Alternative splicing site";
			
			if($e1[$as1] == 1){
				if($junc1 == $regst1[$as1] or $junc1 == $regnd1[$as1]){
					$region1 = "exon_boundary";
				}
				else{
					$region1 = "exon";
				}
			}
			else{
				$region1 = "intron";
			}
			if($e2[$as2] == 1){
				if($junc2 == $regst2[$as2] or $junc2 == $regnd2[$as2]){
					$region2 = "exon_boundary";
				}
				else{
					$region2 = "exon";
				}
			}
			else{
				$region2 = "intron";
			}

			if($region1 eq "exon_boundary" and $region2 eq "exon_boundary"){
				$splicing = "Canonical splicing site";
			}
			
			if(($e1[$as1]>0 and $e2[$as2]>0) or ($filter !~ /I/)){
				$find = 1; 
				my $sample = substr($input,rindex($input,"/")+1);
				print fileWriter2 $sample."\t".$chr1."\t".$chr2."\t".$gene1."\t".$trans1[$as1]."\t".$trst1[$as1]."\t".$trnd1[$as1]."\t".$gene2."\t".$trans2[$as2]."\t".$trst2[$as2]."\t".$trnd2[$as2]."\t".$str1."\t".$str2."\t".$location1[$as1]."\t".$regst1[$as1]."\t".$junc1."\t".$regnd1[$as1]."\t".$location2[$as2]."\t".$regst2[$as2]."\t".$junc2."\t".$regnd2[$as2]."\t".$region1."\t".$region2."\t".$splicing."\t".$anno."\t";
			
	
				if($seq_pass1[$as1] and $seq_pass2[$as2]){
					print fileWriter2 "ok\n";
					$seq = $s1[$as1]."|".$s2[$as2];
					print fileWriter1 ">".$gene1."(".$trans1[$as1]."):".$gene2."(".$trans2[$as2].") chr".$chr1.":".$junc1."(".$location1[$as1].")-chr".$chr2.":".$junc2."(".$location2[$as2].")_".int(index($seq,"|")+1)."\n";
					print fileWriter1 substr($seq,0,index($seq,"|")).substr($seq,index($seq,"|")+1)."\n";
				}
				else{
					print fileWriter2 "none\n";
				}
			}
		}
	}
	return $find;
}
1;


open (fileWriter1, ">".$out_dir."/redundant_fusion_cDNA_".$filter.".fasta") || die "Cannot make new file1\n";
open (fileWriter2, ">".$out_dir."/FusionPro-final_result_".$filter.".tsv") || die "Cannot make new file2\n";
open (fileWriter3, ">".$out_dir."/FusionPro-filtered_out_".$filter.".tsv") || die "Cannot make new file3\n";
open (fileHandler1, $input) || die "Cannot open ".$input.".\n";

my $i="";
my $line="";
my $seq="";
my @noX;
my $numX=0; 

$i=0;
while($line=<fileHandler1>){
	$line =~ s/\R|\n//g;
	if($line ne ""){
		my @array = split /\t/,$line;
		my $chr1=$array[0];
		my $chr2=$array[1];
		my $gene1=$array[2];
		my $gene2=$array[3];
		my $junc1=$array[4];
		my $junc2=$array[5];
		my $str1=$array[6];
		my $str2=$array[7];

		#annotating the fusion
		my $anno = Mitelman_annotation($gene1,$gene2);

		if($filter =~ /P/){
			#check if gene1 sequence is similar with gene2 sequence - paralogs
			my $similar = Ensembl_paralog_filtration($gene1,$gene2);
			if($similar){
				$i++;
				next;
			}
			#check if gene1 sequence is similar with gene2 sequence - pseudogenes
			my $pseudo = Gencode_pseudogene_filtration($gene1,$gene2);
			if($pseudo){
				$i++;
				next;
			}
		}
		my $filtered = fusion_analysis($gene1, $gene2, $chr1, $chr2, $str1, $str2, $junc1, $junc2, $anno);
		if($filtered == 0){
			print fileWriter3 $line."\n";
		}

	}
	$i++;
}


close(fileWriter1);
close(fileWriter2);
close(fileWriter3);
close(fileHandler1);

		
system("perl ".$bin_dir."/FusionPro-Run-redundant_remover.pl redundant_fusion_cDNA_".$filter.".fasta ".$filter." ".$out_dir);
system("perl ".$bin_dir."/FusionPro-Run-three_frame_translator.pl FusionPro-final_fusion_cDNA_".$filter.".fasta ".$filter." ".$out_dir." ".$bin_dir." ".$enzyme);
system("perl ".$bin_dir."/FusionPro-Run-redundant_remover_2.pl ".$filter." ".$out_dir." redundant_fusion_proteoform FusionPro-final_fusion_proteoform");
system("perl ".$bin_dir."/FusionPro-Run-redundant_remover_2.pl ".$filter." ".$out_dir." redundant_fusion_proteotypic_peptide FusionPro-final_proteotypic_peptide");
unlink($out_dir."/redundant_fusion_proteotypic_peptide_".$filter.".fasta");