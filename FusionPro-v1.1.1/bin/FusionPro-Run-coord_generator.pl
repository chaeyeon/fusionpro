#!/usr/bin/perl
use strict;
use warnings;
use List::Util qw/ min max /;

open (fileHandle, $ARGV[0]) || die "Cannot open file\n";
open (fileWriter, ">".$ARGV[1]."/exon_coord.tsv") || die "Cannot make new file\n";

my $line;
my $trans_old="";
my @gene_exst;
my @gene_exnd;
my @trans_id;
my $tran_st;
my $tran_nd;
my $gene_old;
my $chr;
my $trans;
my $five_prime_utr_st;
my $five_prime_utr_nd;
my $three_prime_utr_st;
my $three_prime_utr_nd;
my $ensembl_genome_version = $ARGV[2];

if($ensembl_genome_version eq "GRCh38"){
	while($line=<fileHandle>){
		my @array = split /\t/,$line;
		if(!($array[0] =~ /(\#.*)/)){
			if($array[2] eq "exon"){
				my $exst = $array[3];
				my $exnd = $array[4];
			
				push(@gene_exst,$exst);
				push(@gene_exnd,$exnd);
			}
			elsif($array[2] eq "five_prime_utr"){
				if($five_prime_utr_st == 0){
					$five_prime_utr_st = $array[3];
					$five_prime_utr_nd = $array[4];
				}
				else{
					$five_prime_utr_st = min($array[3],$five_prime_utr_st);
					$five_prime_utr_nd = max($array[4],$five_prime_utr_nd);
				}
			}
			elsif($array[2] eq "three_prime_utr"){
				if($three_prime_utr_st == 0){
					$three_prime_utr_st = $array[3];
					$three_prime_utr_nd = $array[4];
				}
				else{
					$three_prime_utr_st = min($array[3],$three_prime_utr_st);
					$three_prime_utr_nd = max($array[4],$three_prime_utr_nd);
				}
			}
			elsif($array[2] eq "transcript"){
				my @features = split /;/, $array[8];
				my $gene;
				
				foreach my $feature (@features){
					if ($feature =~ /(\S+)\s+(.*)/){
						my $key = $1;
						my $value = $2;
						$value =~ s/"//g;
						$gene = $value if $key eq "gene_name";
						$trans = $value if $key eq "transcript_id";
					}
				}
				if(!($trans_old eq "") and !($trans eq $trans_old)){
					print fileWriter $gene_old."	".$chr."	";

					foreach my $x (@gene_exst) {
						print fileWriter $x.",";
					}
					print fileWriter "	";
					foreach my $y (@gene_exnd) {
						print fileWriter  $y.",";
					}
					print fileWriter "	".$trans_old."	".$tran_st."	".$tran_nd;
					print fileWriter "	".$five_prime_utr_st."	".$five_prime_utr_nd. "	".$three_prime_utr_st."	".$three_prime_utr_nd;			
					print fileWriter "\n";

					@gene_exst = ( );
					@gene_exnd = ( );
				} 	
				$trans_old = $trans;
				$gene_old  = $gene;
				$chr = $array[0];
				$tran_st = $array[3];
				$tran_nd = $array[4];
				$five_prime_utr_st=0;
				$five_prime_utr_nd=0;
				$three_prime_utr_st=0;
				$three_prime_utr_nd=0;
			}
		}
	}
}
else{
	while($line=<fileHandle>){
		my @array = split /\t/,$line;
		if(!($array[0] =~ /(\#.*)/)){
			if($array[2] eq "exon"){
				my $exst = $array[3];
				my $exnd = $array[4];

				my @features = split /;/, $array[8];
				my $gene;
				
				foreach my $feature (@features){
					if ($feature =~ /(\S+)\s+(.*)/){
						my $key = $1;
						my $value = $2;
						$value =~ s/"//g;
						$gene = $value if $key eq "gene_name";
						$trans = $value if $key eq "transcript_id";
					}
				}
				if(!($trans_old eq "") and !($trans eq $trans_old)){
					print fileWriter $gene_old."	".$chr."	";

					foreach my $x (@gene_exst) {
						print fileWriter $x.",";
					}
					print fileWriter "	";
					foreach my $y (@gene_exnd) {
						print fileWriter  $y.",";
					}
					print fileWriter "	".$trans_old."	".$gene_exst[0]."	".$gene_exnd[-1];
					print fileWriter "	".$five_prime_utr_st."	".$five_prime_utr_nd. "	".$three_prime_utr_st."	".$three_prime_utr_nd;		
					print fileWriter "\n";

					$trans_old = $trans;
					$gene_old  = $gene;
					$chr = $array[0];
					$five_prime_utr_st=0;
					$five_prime_utr_nd=0;
					$three_prime_utr_st=0;
					$three_prime_utr_nd=0;
					@gene_exst = ( );
					@gene_exnd = ( );
				} 
				elsif($trans_old eq ""){
					$trans_old = $trans;
					$gene_old  = $gene;
					$chr = $array[0];
					$five_prime_utr_st=0;
					$five_prime_utr_nd=0;
					$three_prime_utr_st=0;
					$three_prime_utr_nd=0;
				}
				push(@gene_exst,$exst);
				push(@gene_exnd,$exnd);
			}
		}
	}
}


print fileWriter $gene_old."	".$chr."	";

foreach my $x (@gene_exst) {
	print fileWriter $x.",";
}
print fileWriter "	";
foreach my $y (@gene_exnd) {
	print fileWriter  $y.",";
}
print fileWriter "	".$trans_old."	".$gene_exst[0]."	".$gene_exnd[-1];

unless($five_prime_utr_st eq ""){
	print fileWriter "	".$five_prime_utr_st."	".$five_prime_utr_nd;
}
else{
	print fileWriter "	-	-";
}
unless($three_prime_utr_st eq ""){
	print fileWriter "	".$three_prime_utr_st."	".$three_prime_utr_nd;
}
else{
	print fileWriter "	-	-";
}

print fileWriter "\n";



close(fileHandle);
close(fileWriter);
