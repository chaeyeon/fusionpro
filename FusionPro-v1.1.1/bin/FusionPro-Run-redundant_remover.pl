#!/usr/bin/perl
use strict;
use warnings;

my $fileName = $ARGV[2]."/".$ARGV[0];
my $filter = $ARGV[1];

open (fileWriter, ">".$ARGV[2]."/FusionPro-final_fusion_cDNA_".$filter.".fasta") || die "Cannot make new file\n";
open (fileHandle, $fileName) || die "Cannot open ".$fileName.".\n";

my $red=0;
my $line;
my $anno;
my %fusion;
while($line=<fileHandle>){
	chomp $line;
	if($line =~ />/){
		$anno = "";
		$red = 0;

		#FusionPro-C case
		if(index($line,"|") == -1){
			$anno = substr($line,index($line,">")+1);
		}
		else{
			$anno = substr($line,index($line,"|")+1);
		}

		if(exists($fusion{$anno})){
			$red = 1;
		}
		
	}
	else {
		if($red==0){
			foreach my $key (keys %fusion) {
				if($key eq $line){
					$red = 1;
				}
			}
		}

		if($red==0){
			#FusionPro-C case
			
			$fusion{$anno} = $line;
			print fileWriter ">".$anno."\n".$line."\n";
		}
	}
}
		
close(fileWriter);
close(fileHandle);
