use strict;
use warnings;

my ($enzyme, $anno, $fupep, $fujunc, $output) = @ARGV;


open(fileWriter, ">>".$output) || die "Cannot make new file.\n";

my $max_mc = 2;
my $max_len = 50;
my $min_len = 7;

my @peps;
my $num_peps;
my $pos=0;
my $seq=$fupep;
my $pep="";
my $cut="";
for ($num_peps=1; length($seq)>0; $num_peps++){
	if($enzyme eq "trypsin"){
		$seq =~ /([KR][^P]?)/;
		$pos = $-[0];
		if (defined $pos){
			$peps[$num_peps] = substr($seq, 0, $pos+1);
			$seq = substr($seq, $pos+1);
		}
		else{
			$seq = "";
		}
	}
	elsif($enzyme eq "chymotrypsin"){
		$seq =~ /([FLWY][^P]?)/;
		$pos = $-[0];
		if (defined $pos){
			$peps[$num_peps] = substr($seq, 0, $pos+1);
			$seq = substr($seq, $pos+1);
		}
		else{
			$seq = "";
		}
	}
	elsif($enzyme eq "Asp-N"){
		$seq =~ /([D])/;
		$pos = $-[0];
		if (defined $pos){
			$peps[$num_peps] = $cut.substr($seq, 0, $pos);
			$cut = substr($seq,$pos,1);
			$seq = substr($seq,$pos+1);
		}
		else{
			$seq = "";
		}
	}
	else{
		$seq =~ /([KR][^P]?)/;
		$pos = $-[0];
		if (defined $pos){
			$peps[$num_peps] = substr($seq, 0, $pos+1);
			$seq = substr($seq, $pos+1);
		}
		else{
			$seq = "";
		}
	}
}


$num_peps--;
foreach my $mc (0..$max_mc){
	for(my $i=0; $i<=($num_peps-$mc); $i++){
		$pep = $peps[$i];
		
		if(defined $pep){
			my $len = length($pep);	
	
			for(my $j=1; $j <= $mc; $j++){
				if(defined $pep and defined $peps[$i+$j]){
					$pep = $pep.$peps[$i+$j];
				}
			}
		
			if($pep =~ /\*/){
				$pep = substr $pep, 0, index ($pep, "*");
			}
		
			$len = length($pep);	

			if($len <= $max_len && $len >= $min_len && $pep =~ /$fujunc/){
				print fileWriter ">".$anno."_".$enzyme."\n".$pep."\n";
			}	
		}

	}
}


close(fileWriter);