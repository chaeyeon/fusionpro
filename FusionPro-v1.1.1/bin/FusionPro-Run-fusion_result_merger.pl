#!/usr/bin/perl
use strict;
use warnings;
use lib $ARGV[0];

my $fileName1 = $ARGV[2].".final.Fusion.specific.for.genes";	#SOAPfuse result
my $fileName2 = "fusions_well_annotated.txt";					#MapSplice2 result
my $fileName3 = "potential_fusion.txt";							#TopHat-fusion-post result

my %fusions;

#Processing result of SOAPfuse
open (fileHandler1, $fileName1) || die "Cannot open ".$fileName1.".\n";
my $str1="";
my $str2="";
my $chr1="";
my $chr2="";
my $gene1="";
my $gene2="";
my $junc1="";
my $junc2="";
my $span_read=0;
my $span_pair=0;
my $program="";
my $line="";
my $i=0;
while($line=<fileHandler1>){
	$line =~ s/\R|\n//g;
	if($i!=0 && !($line eq "")){
		my @array = split /\t/,$line;
		
		$gene1=$array[0];
		$gene2=$array[5];
		$str1=$array[2];
		$str2=$array[7];
		$chr1=substr($array[1],3);
		$chr2=substr($array[6],3);
		$span_read=$array[11];
		$span_pair=$array[10];

		my $junc1=$array[3];
		my $junc2=$array[8];
		
		$program = "S";
		$fusions{$gene1.$gene2.$junc1.$junc2} = join ("\t", ($gene1, $gene2, $chr1, $chr2, $str1, $str2, $junc1, $junc2, $span_read, $span_pair, $program))
		
	}
	elsif($i==0){
		$i++;
	}
}
close(fileHandler1);

open (fileHandler2, $fileName2) || die "Cannot open ".$fileName2.".\n";
#Processing result of MapSplice2
while($line=<fileHandler2>){
	$line =~ s/\R|\n//g;
	if(!($line eq "")){
		my @array = split /\t/,$line;
		my @g1 = split /,/,$array[60];
		$gene1=$g1[0];
		my @g2 = split /,/,$array[61];
		$gene2=$g2[0];

		my @str = split /,/,$array[59];
		$str1=substr($str[0],0,1);
		$str2=substr($str[1],0,1);

		$span_read=$array[21];
		$span_pair=$array[27];

		$junc1 = $array[1];
		$junc2 = $array[2];

		my @c = split /~/,$array[0];
		$chr1=$c[0];
		$chr2=$c[1];

		#check the supporting reads
		#[19] = unique_read_count
		#[22],[23] = left,right_paired_read_count
		#[27] = encompassing_read_pair_count (but not cross the fusion)
		#if($array[19]==0 or ($array[22]+$array[23]+$array[27]==0)){
		#	next;
		#}
		
		if(exists $fusions{$gene1.$gene2.$junc1.$junc2}){
			$fusions{$gene1.$gene2.$junc1.$junc2} = $fusions{$gene1.$gene2.$junc1.$junc2}."M";
		}
		else{
			$program = "M";
			$fusions{$gene1.$gene2.$junc1.$junc2} = join ("\t", ($gene1, $gene2, $chr1, $chr2, $str1, $str2, $junc1, $junc2, $span_read, $span_pair, $program))

		}
	}

}
close(fileHandler2);


#Processing result of Tophat-fusion (potential_fusion.txt, result.txt)
open (fileHandler3, $fileName3) || die "Cannot open ".$fileName3.".\n";
$i=0;
my $des="";
my $str;

while($line=<fileHandler3>){
	$line =~ s/\R|\n//g;
	if($i%6 eq 0){
		my @array= split / /,$line;

		$str=$array[4];
		if(substr($str,0,1) eq "f"){
			$str1 = "+";
		}
		else{
			$str1 = "-";
		}
		if(substr($str,1,1) eq "f"){
			$str2 = "+";
		}
		else{
			$str2 = "-";
		}

		my @chr=split /-/,$array[1];	
		$chr1=$chr[0];
		$chr2=$chr[1];

		$junc1 = $array[2]+1;
		$junc2 = $array[3]+1;
	}
	elsif($i%6 eq 4){
		my @array= split / /,$line;
		$gene1 = $array[0];
		$gene2 = $array[2];	
		
		#annotating the fusion
		#matching tophat results(potential_fusion.txt) with final candidate list(result.txt)
		my $r_l;
		open (fileHandler4, "result.txt") || die "Cannot open result.txt.\n";
		while($r_l=<fileHandler4>){
			my @a = split /	/,$r_l;
			if(($a[1] eq $gene1) and ($a[4] eq $gene2) and ($a[3] eq ($junc1-1)) and ($a[6] eq ($junc2-1))){
				$span_read=$a[7]+$a[9];
				$span_pair=$a[8];

				if(exists $fusions{$gene1.$gene2.$junc1.$junc2}){
					$fusions{$gene1.$gene2.$junc1.$junc2} = $fusions{$gene1.$gene2.$junc1.$junc2}."T";
				}
				else{
					$program = "T";
					$fusions{$gene1.$gene2.$junc1.$junc2} = join ("\t", ($gene1, $gene2, $chr1, $chr2, $str1, $str2, $junc1, $junc2, $span_read, $span_pair, $program))

				}

				last;
			}
		}
		close(fileHandler4);
	}
	$i++;
}
close(fileHandler3);




open (fileWriter, ">".$ARGV[1]."/merged.tmp") || die "Cannot make new file1\n";

my @vals = values %fusions;
print fileWriter "$_\n" foreach (@vals);

close(fileWriter);

unlink($ARGV[1]."/merged.tmp")