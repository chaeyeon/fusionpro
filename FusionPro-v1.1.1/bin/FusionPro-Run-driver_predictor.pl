#!/usr/bin/perl
#driver_predictor.pl
use strict;
use warnings;

my $fileName1= $ARGV[1]."/oncofuse_out_".$ARGV[0].".txt";
open (fileHandle1, $fileName1) || die "Cannot open $fileName1.\n";
open (fileWriter, ">".$ARGV[1]."/FusionPro-driver_prediction_".$ARGV[0].".txt") || die "Cannot make new file\n";

my $line;
my $i=0;

while($line=<fileHandle1>){
	if($i!=0){
		my @array= split /\t/,$line;
		if($array[22]>0.5 and $array[21]!=1){
			print fileWriter $array[5]."\t".$array[6]."\t".$array[13]."\t";
			
			#for($i=6;$i<20;$i++){
			#	if($i!=11 && $i!=18){
			#		print fileWriter $array[$i]."\t";
			#	}
			#}
			#print fileWriter "\n";
			print fileWriter $array[21]."\t".$array[22]."\n";
		}
	}
	else{
		print fileWriter "location\t5'gene\t3'gene\tp_val_corr\tdriver_prob\n";
	}
	$i++;
}

close(fileHandle1);
close(fileWriter);

