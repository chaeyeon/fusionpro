use strict;
use warnings;
use lib $ARGV[0];
use style;

#$ARGV[0] = bin_dir
my $bin_dir = $ARGV[0];
my $filter = $ARGV[1];
my $program;
my $gene1;
my $gene2;
my $i;
my $l;
my @SOAPfuse;
my @MapSplice2;
my @TopHat2;
open (fileHandler1, $ARGV[2]."/FusionPro-merged_result_".$filter.".tsv") || die "Cannot open FusionPro-merged_result_".$filter.".tsv.\n";
while($l=<fileHandler1>){
	$l =~ s/\R|\n//g;
	my @array = split /	/,$l;
	$program = $array[28];
	$gene1 = $array[3];
	$gene2 = $array[7];
	 
	if($program =~ /SOAPfuse/){
		if ((!grep { $_ eq $gene1.":".$gene2 } @SOAPfuse) and (!grep { $_ eq $gene2.":".$gene1 } @SOAPfuse)) {
			push @SOAPfuse, $gene1.":".$gene2;
		}
	}
	if($program =~ /MapSplice2S/){
		if ((!grep { $_ eq $gene1.":".$gene2 } @MapSplice2) and (!grep { $_ eq $gene2.":".$gene1 } @MapSplice2)) {
			push @MapSplice2, $gene1.":".$gene2;
		}
	}
	if($program =~ /TopHat2S/){
		if ((!grep { $_ eq $gene1.":".$gene2 } @TopHat2) and (!grep { $_ eq $gene2.":".$gene1 } @TopHat2)) {
			push @TopHat2, $gene1.":".$gene2;
		}
	}
}
close(fileHandler1);

my $style_return = style::css($bin_dir."/style.css");
my $S=@SOAPfuse;
my $M=@MapSplice2;
my $T=@TopHat2;
my @SOAPfuse_temp = @SOAPfuse;
my @TopHat2_temp = @TopHat2;
my @MapSplice2_temp = @MapSplice2;
my @DM_list;
my @DT_list;
my @MT_list;
my @DMT_list;
my $SM=0;
my $ST=0;
my $MT=0;
my $SMT=0;
my $total=0;

foreach my $fusion (@SOAPfuse_temp) {
	#3개 전부 공통
	my $reverse_fusion = substr($fusion,index($fusion,":")+1).":".substr($fusion,0,index($fusion,":"));
	my $Map_intersection=0;
	my @temp = grep {$_ eq $fusion} @MapSplice2_temp;
	if(@temp){
		$Map_intersection++;
	}
	@temp = grep {$_ eq $reverse_fusion} @MapSplice2_temp;
	if(@temp){
		$Map_intersection++;
	}
	my $Top_intersection=0;
	@temp = grep {$_ eq $fusion} @TopHat2_temp;
	if(@temp){
		$Top_intersection++;
	}
	@temp = grep {$_ eq $reverse_fusion} @TopHat2_temp;
	if(@temp){
		$Top_intersection++;
	}

	if (($Map_intersection>0) and ($Top_intersection>0)) {
		$SMT++;
		$S--;
		$M--;
		$T--;
		push @DMT_list, $fusion;
		$i=0;
		foreach my $e (@SOAPfuse) {
			if(($e eq $fusion) or ($e eq $reverse_fusion)){
				splice @SOAPfuse, $i, 1;
			}
			$i++;
		}
		$i=0;
		foreach my $e (@MapSplice2) {
			if(($e eq $fusion) or ($e eq $reverse_fusion)){
				splice @MapSplice2, $i, 1;
			}
			$i++;
		}
		$i=0;
		foreach my $e (@TopHat2) {
			if(($e eq $fusion) or ($e eq $reverse_fusion)){
				splice @TopHat2, $i, 1;
			}
			$i++;
		}
	}
	#SOAPfuse와 mapsplice2
	elsif (($Map_intersection>0) and ($Top_intersection==0)) {
		$SM++;
		$S--;
		$M--;
		push @DM_list, $fusion;
		$i=0;
		foreach my $e (@SOAPfuse) {
			if(($e eq $fusion) or ($e eq $reverse_fusion)){
				splice @SOAPfuse, $i, 1;
			}
			$i++;
		}
		$i=0;
		foreach my $e (@MapSplice2) {
			if(($e eq $fusion) or ($e eq $reverse_fusion)){
				splice @MapSplice2, $i, 1;
			}
			$i++;
		}
	}
	#SOAPfuse와 tophat2
	elsif(($Map_intersection==0) and ($Top_intersection>0)) {
		$ST++;
		$S--;
		$T--;
		push @DT_list, $fusion;
		$i=0;
		foreach my $e (@SOAPfuse) {
			if(($e eq $fusion) or ($e eq $reverse_fusion)){
				splice @SOAPfuse, $i, 1;
			}
			$i++;
		}
		$i=0;
		foreach my $e (@TopHat2) {
			if(($e eq $fusion) or ($e eq $reverse_fusion)){
				splice @TopHat2, $i, 1;
			}
			$i++;
		}
	}
}

foreach my $fusion (@TopHat2_temp) {
	#tophat과 mapsplice
	my $reverse_fusion = substr($fusion,index($fusion,":")+1).":".substr($fusion,0,index($fusion,":"));

	my $Map_intersection=0;
	my @temp = grep {$_ eq $fusion} @MapSplice2_temp;
	if(@temp){
		$Map_intersection++;
	}
	@temp = grep {$_ eq $reverse_fusion} @MapSplice2_temp;
	if(@temp){
		$Map_intersection++;
	}
	my $Soa_intersection=0;
	@temp = grep {$_ eq $fusion} @SOAPfuse_temp;
	if(@temp){
		$Soa_intersection++;
	}
	@temp = grep {$_ eq $reverse_fusion} @SOAPfuse_temp;
	if(@temp){
		$Soa_intersection++;
	}

	if (($Map_intersection>0) and ($Soa_intersection==0)) {
		$MT++;
		$M--;
		$T--;
		push @MT_list, $fusion;
		$i=0;
		foreach my $e (@TopHat2) {
			if(($e eq $fusion) or ($e eq $reverse_fusion)){
				splice @TopHat2, $i, 1;
			}
			$i++;
		}
		$i=0;
		foreach my $e (@MapSplice2) {
			if(($e eq $fusion) or ($e eq $reverse_fusion)){
				splice @MapSplice2, $i, 1;
			}
			$i++;
		}
	}
}
$total = $S+$M+$T+$SM+$ST+$MT+$SMT;

open (fileWriter1, ">".$ARGV[2]."/FusionPro-fusion_transcript_prediction_summary_".$filter.".html") || die "Cannot make new file\n";

print fileWriter1 "<head>\n <style type=\"text/css\">\n";
print fileWriter1 $style_return."\n";
print fileWriter1 "</style></head>\n<body>\n";
print fileWriter1 "<div id=\"first\" class=\"circle\"><div id=\"text1\" class=\"text\">".$S."</div>\n";
print fileWriter1 "<div id=\"text4\" class=\"text\">".$ST."</div><div id=\"text5\" class=\"text\">".$SM."</div></div>\n";
print fileWriter1 "<div id=\"second\" class=\"circle\"><div id=\"text2\" class=\"text\">".$T."</div>\n";
print fileWriter1 "<div id=\"text6\" class=\"text\">".$MT."</div><div id=\"text7\" class=\"text\">".$SMT."</div></div>\n";
print fileWriter1 "<div id=\"third\" class=\"circle\"><div id=\"text3\" class=\"text\">".$M."</div></div>\n";
print fileWriter1 "<div id=\"caption\">Red: SOAPfuse / Green: TopHat2S / Blue: MapSplice2S\n";

print fileWriter1 "<table>\n";
print fileWriter1 "<tr><th>SOAPfuse (".$S.")</th><td>\n";
foreach my $e (@SOAPfuse) {
	print fileWriter1 $e.", ";
}
print fileWriter1 "</td></tr>\n";

print fileWriter1 "<tr class=\"even\"><th>MapSplice2S (".$M.")</th><td>\n";
foreach my $e (@MapSplice2) {
	print fileWriter1 $e.", ";
}
print fileWriter1 "</td></tr>\n";

print fileWriter1 "<tr><th>TopHat2S (".$T.")</th><td>\n";
foreach my $e (@TopHat2) {
	print fileWriter1 $e.", ";
}
print fileWriter1 "</td></tr>\n";

print fileWriter1 "<tr class=\"even\"><th>SOAPfuse/Mapsplice2S (".$SM.")</th><td>\n";
foreach my $e (@DM_list) {
	print fileWriter1 $e.", ";
}
print fileWriter1 "</td></tr>\n";

print fileWriter1 "<tr><th>SOAPfuse/TopHat2S (".$ST.")</th><td>\n";
foreach my $e (@DT_list) {
	print fileWriter1 $e.", ";
}
print fileWriter1 "</td></tr>\n";

print fileWriter1 "<tr class=\"even\"><th>MapSplice2S/TopHat2S (".$MT.")</th><td>\n";
foreach my $e (@MT_list) {
	print fileWriter1 $e.", ";
}
print fileWriter1 "</td></tr>\n";

print fileWriter1 "<tr><th>SOAPfuse/MapSplice2S/TopHat2S (".$SMT.")</th><td>\n";
foreach my $e (@DMT_list) {
	print fileWriter1 $e.", ";
}
print fileWriter1 "</td></tr>\n";

print fileWriter1 "<tr  class=\"even\"><th colspan=\"2\">Total: ".$total."</th></tr>\n";
print fileWriter1 "</table>\n</body>";

close(fileWriter1);
