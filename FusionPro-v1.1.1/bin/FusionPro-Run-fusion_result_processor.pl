#===============================================================================#
#		Filtering out false fusion genes and Making final results				#
#		=> Processing results of SOAPfuse, MapSplice2, TopHat-fusion-post		#	
#===============================================================================#
#!/usr/bin/perl
use strict;
use warnings;
use lib $ARGV[0];
use List::Util qw/ min max /;
use Simple;
use sequence_extractor;
use breakpoint_sequence_extractor;

my %paralog;
my %pseudo;
my %gfpkm;
my %tfpkm;
my %seqs;
my $ref_dir = $ARGV[1];
my $mapsplice_fa = $ARGV[1]."/mapsplice_fa";
my $ensembl_ver = $ARGV[2];
my $filter = $ARGV[3];
my $out_dir = $ARGV[4];
my $cuff_dir = $ARGV[6];
my $sample = $ARGV[7];

#%paralog hash 에 다 넣음
sub Ensembl_paralog{
	open (fileHandler, $ARGV[0]."/../library/Ensembl_paralog_list.tsv") || die "Cannot open Ensembl_paralog_list.tsv\n";
	my $l;
	while($l=<fileHandler>){
		$l =~ s/\R|\n//g;
		my @a = split /\t/,$l;
		$paralog{$a[0]} = $l;
	}
	close (fileHandler);
}


#%pseudo hash 에 다 넣음
sub Gencode_pseudogene{
	open (fileHandler, $ARGV[0]."/../library/gencode.v10.pgene.parents_list.txt") || die "Cannot open gencode.v10.pgene.parents_list.txt\n";
	my $l;
	while($l=<fileHandler>){
		$l =~ s/\R|\n//g;
		my @a = split /\t/,$l;
		if($a[0] and $a[1]){ 
			$pseudo{$a[0]} = $a[1];
			$pseudo{$a[1]} = $a[0];
		}
	}
	close (fileHandler);
}

#%gfpkm hash 에 다 넣음
sub gene_expression{
	open (fileHandler, $cuff_dir."/genes.fpkm_tracking") || die "Cannot open ".$cuff_dir."/genes.fpkm_tracking\n";
	my $l;
	while($l=<fileHandler>){
		my @a = split /\t/,$l;
		my $gene = $a[4];
		my $chr = ($a[6] =~ m/(\s):\s/); 	
		my $fpkm = $a[9];
		$gfpkm{$gene.$chr} = $fpkm;
	}
	close(fileHandler);
}

#%tfpkm hash 에 다 넣음
sub transcript_expression{
	open (fileHandler, $cuff_dir."/isoforms.fpkm_tracking") || die "Cannot open ".$cuff_dir."/isoforms.fpkm_tracking\n";
	my $l;
	while($l=<fileHandler>){
		my @a = split /\t/,$l;
		my $trid = $a[0];
		my $fpkm = $a[9];
	}
	close(fileHandler);
}


#$_[0] = $gene1
#$_[1] = $gene2
sub Mitelman_annotation{
	my $gene1 = $_[0];
	my $gene2 = $_[1];
	my $anno;
	my $url = "https://mitelmandatabase.isb-cgc.org/result?search_type=mb&gene_list=".$gene1."/".$gene2;
	my $content = get $url;
	if(!$content){
		#connection error
		$anno = "error\t."
	}
	
	if($content =~ /$gene1/){
		$anno = "known\thttps://mitelmandatabase.isb-cgc.org/result?search_type=mb&gene_list=".$gene1."/".$gene2.;
	}
	else{
		$anno = "novel\t.";
	}

	return $anno;
}

sub sequence_hash{
	my $seq = "";
	my $frame = "";
	my $id = "";
	my $i= 0;
	my $l;
	open(fileHandler, $ref_dir."/Homo_sapiens.".$ensembl_ver.".cdna.ncrna.chromosomes.fa") || die("Cannot open Homo_sapiens.".$_[2].".cdna.ncrna.chromosomes.fa\n");
	while($l=<fileHandler>){	
		$l =~ s/\R|\n//g;
		if($l =~ />/){
			if($i){
				$seqs{$id.$frame} = $seq;
				$seq = "";
			}

			$frame="";
			$id = substr($l,1,index($l," ")-1);

			if($l =~ /:([-]*1) gene/){
				if($1 eq "-1"){
					$frame = "-";
				}
				elsif($1 eq "1"){
					$frame = "+";
				}
			}
			$i++;
		}
		else{
			$seq .= $l;
		}
	}
	$seqs{$id.$frame} = $seq;

	close(fileHandler);
}

sub fusion_analysis{
	my $fileName4 = "exon_coord.tsv";						#transcripts start, end
	my $gene1 = $_[0];
	my $gene2 = $_[1];
	my $chr1 = $_[2];
	my $chr2 = $_[3];
	my $str1 = $_[4];
	my $str2 = $_[5];
	my $junc1 = $_[6];
	my $junc2 = $_[7];
	my $span_read = $_[8];
	my $span_pair = $_[9];
	my $anno = $_[10];
	my $program = $_[11];
	my $cutoff = 0;

	#check if gene1 and gene2 sequences are similar 
	if($filter =~ /P/){
		#paralog check
		if($paralog{$gene1}){
			if($paralog{$gene1} =~  /[\t]*$gene2[\t]*/){
				return 0;
			}
		}
		if($paralog{$gene2}){
			if($paralog{$gene2} =~  /[\t]*$gene1[\t]*/){
				return 0;
			}
		}
		
		#pseudogene check
		if($pseudo{$gene1}){
			if($pseudo{$gene1} eq $gene2){
				return 0;
			}
		}
		if($pseudo{$gene2}){
			if($pseudo{$gene2} eq $gene1){
				return 0;
			}
		}
	}
	
	#check if gene1 or gene2 have low fpkm value below the cutoff
	if($filter =~ /X/){
		if($gfpkm{$gene1.$chr1} and $gfpkm{$gene1.$chr1}){
			if (int($gfpkm{$gene1.$chr1}) <= $cutoff or int($gfpkm{$gene1.$chr1}) <= $cutoff) {
				return 0;
			}
		}
	}

	#check gene1 - exon: $e1=1 / intron: $e1=0
	my $l;
	my @e1;
	my @e2;
	my @s1;
	my @s2;
	my @seq_pass1;
	my @seq_pass2;
	my @trans1;
	my @trans2;
	my @regst1;
	my @regnd1;
	my @regst2;
	my @regnd2;
	my $break1="";
	my $break2="";
	my $exst1=".";
	my $exnd1=".";
	my $exst2=".";
	my $exnd2=".";
	my @trst1;
	my @trnd1;
	my @trst2;
	my @trnd2;
	my $trst1=0;
	my $trnd1=0;
	my $trst2=0;
	my $trnd2=0;
	my @exst;
	my @exnd;
	my @location1;
	my @location2;
	my $find = 0;


	#check gene1 - exon: $e1=1 / intron: $e1=0
	open (fileHandler4, $fileName4) || die "Cannot open ".$fileName4.".\n";
	while($l=<fileHandler4>){
		$l =~ s/\R|\n//g;
		my @arr = split /\t/,$l;
		my $gene = $arr[0];
		my $chr = $arr[1];
		my $exon_junc_len = 0;
		my $intron_junc_len = 0;
		my $e = 0;
		my $trans_id = $arr[4];
		$trans_id =~ s/\R|\n//g;
		
		if(($gene1 eq $gene) and ($chr1 eq $chr)) {
			$find = 1;
			if($filter =~ /X/){
				if($tfpkm{$trans_id}){
					if (int($tfpkm{$trans_id}) <= $cutoff) {
						next;
					}
				}
			}
						
			@exst = ( );
			@exnd = ( );
			push @exst, split /,/,substr($arr[2],0,length($arr[2])-1);
			push @exnd, split /,/,substr($arr[3],0,length($arr[3])-1);
			@exst= sort {$a <=> $b} @exst;
			@exnd= sort {$a <=> $b} @exnd;

			my $j=0;
			
			$exst1 = ".";
			$exnd1 = ".";
			$trst1 = int($arr[5]);
			$trnd1 = int($arr[6]);

			push @trst1, $trst1;
			push @trnd1, $trnd1;
		
			my $location ="";
			for ($j=0; $j<@exst; $j++){
				if($exst[$j]<=$junc1){
					if($exnd[$j]>=$junc1){
						$e = 1;
						if($str1 eq "+"){
							$location = "exon".($j+1);
						}
						else{
							$location = "exon".(@exst-$j);
						}
						$exst1=$exst[$j];
						$exnd1=$exnd[$j];
						$exon_junc_len += $junc1-$exst[$j]+1;
						last;
					}
					elsif($j == @exst-1){
						$exst1=$exnd[$j]+1;
						$exnd1=$trnd1;
						$intron_junc_len = $junc1-$exnd[$j]+1;
						$location = "intron";
						last;
					}
					else{
						$exon_junc_len += ($exnd[$j]-$exst[$j]+1);
					}
				}
				elsif($e != 1){
					if ($j != 0 and $exnd[$j-1]<$junc1) {
						#$exst and $exnd is for intron start and end coord.
						$exst1=$exnd[$j-1]+1;
						$exnd1=$exst[$j]-1;
						$intron_junc_len = $junc1-$exst1+1;
						$location = "intron";
						last;
					}
					elsif ($j==0) {
						$exst1=$trst1;
						$exnd1=$exst[$j]-1;
						$intron_junc_len = $junc1-$trst1+1; 
						$location = "intron";
						last;
					}
				}

			}
			if($arr[7] && $arr[8]){
				if(($arr[7]<=$junc1) and ($arr[8]>=$junc1)){
					$e=1;

					$location = "5'utr";
					$exst1=$arr[7];
					$exnd1=$arr[8];
				}
			}
			if($arr[9] && $arr[10]){
				if(($arr[9]<=$junc1) and ($arr[10]>=$junc1)){
					$e=1;

					$location = "3'utr";
					$exst1=$arr[9];
					$exnd1=$arr[10];
				}
			}

			if($filter =~ /I/){
				if($location eq "intron"){
					pop @trst1;
					pop @trnd1;
					next;
				}
			}

			push @location1, $location;

			if($exst[0]>$trst1){
				$exon_junc_len += $exst[0]-$trst1+1;
			}
			if($exnd[@exnd-1]<$trnd1){
				$exon_junc_len += $trnd1-$exnd[@exnd-1]+1;
			}

			push @regst1, $exst1;
			push @regnd1, $exnd1;

			
			if($filter !~ /I/){
				my $s1_return;
				if($e){
					$s1_return = sequence_extractor::cdna_sequencing($str1, $ref_dir, $ensembl_ver, $trans_id, $exon_junc_len, "5", $str1.$str2, \%seqs);
					if (length($s1_return) >= 15 and $s1_return ne "no sequence"){
						push @seq_pass1, 1;
					}
					else{
						push @seq_pass1, 0;
					}
				}
				else{
					my $s1_cdna = sequence_extractor::cdna_sequencing($str1, $ref_dir, $ensembl_ver, $trans_id, $exon_junc_len, "5", $str1.$str2, \%seqs);
					my $s1_intron = sequence_extractor::intron_region_sequencing($mapsplice_fa, $junc1, $str1, $chr1, "5", $intron_junc_len);
					
					if($s1_cdna eq "no sequence"){
						$s1_cdna = "";
					}
					$s1_return = $s1_cdna.$s1_intron;

					if (length($s1_return) >= 15){
						push @seq_pass1, 1;				
					}
					else{
						push @seq_pass1, 0;
					}
				}
				push @trans1, $trans_id;
				push @e1, $e;
				push @s1, $s1_return;			
			}
			elsif($e){
				my $s1_return = sequence_extractor::cdna_sequencing($str1, $ref_dir, $ensembl_ver, $trans_id, $exon_junc_len, "5", $str1.$str2, \%seqs);
				if (length($s1_return) >= 15 and $s1_return ne "no sequence"){
					push @seq_pass1, 1;
				}
				else{
					push @seq_pass1, 0;
				}
				push @trans1, $trans_id;
				push @e1, $e;
				push @s1, $s1_return;					
			}
		}
	}
	close(fileHandler4);


	if($find == 0){
		print "Warning!!! There is no Ensembl exon coordinates for gene ".$gene1.". Skipped.\n";
		return 0;
	}
	
	$find = 0;
	#check gene2 - exon: $e2=1 / intron: $e2=0
	open (fileHandler4, $fileName4) || die "Cannot open ".$fileName4.".\n";
	while($l=<fileHandler4>){
		$l =~ s/\R|\n//g;
		my @arr = split /\t/,$l;
		my $gene = $arr[0];
		my $chr = $arr[1];
		my $exon_junc_len = 0;
		my $intron_junc_len = 0;
		my $e = 0;
		my $trans_id = $arr[4];
		$trans_id =~ s/\R|\n//g;

		if(($gene2 eq $gene) and ($chr2 eq $chr)) {
			$find = 1;
			if($filter =~ /X/){
				if($tfpkm{$trans_id}){
					if (int($tfpkm{$trans_id}) <= $cutoff) {
						next;
					}
				}
			}

			@exst = ( );
			@exnd = ( );
			push @exst, split /,/,substr($arr[2],0,length($arr[2])-1);
			push @exnd, split /,/,substr($arr[3],0,length($arr[3])-1);
			@exst= sort {$a <=> $b} @exst;
			@exnd= sort {$a <=> $b} @exnd;


			my $j=0;

			$exst2 = ".";
			$exnd2 = ".";
			$trst2 = int($arr[5]);
			$trnd2 = int($arr[6]);

			push @trst2, $trst2;
			push @trnd2, $trnd2;
		
			my $location ="";
			for ($j=0; $j<@exst; $j++){
				if($exst[$j]<=$junc2){
					if($exnd[$j]>=$junc2){
						$e = 1;
						if($str2 eq "+"){
							$location = "exon".($j+1);
						}
						else{
							$location = "exon".(@exst-$j);
						}
						$exst2=$exst[$j];
						$exnd2=$exnd[$j];
						$exon_junc_len += $junc2-$exst[$j]+1;
						last;
					}
					elsif($j == @exst-1){
						$exst2=$exnd[$j]+1;
						$exnd2=$trnd2;
						$intron_junc_len = $junc2-$exnd[$j]+1;
						$location = "intron";
						last;
					}
					else{
						$exon_junc_len += ($exnd[$j]-$exst[$j]+1);
					}
				}
				elsif($e != 1){
					if ($j != 0 and $exnd[$j-1]<$junc2) {
						#$exst and $exnd is for intron start and end coord.
						$exst2=$exnd[$j-1]+1;
						$exnd2=$exst[$j]-1;
						$intron_junc_len = $junc2-$exst2+1;
						$location = "intron";
						last;
					}
					elsif ($j==0) {
						$exst2=$trst2;
						$exnd2=$exst[$j]-1;
						$intron_junc_len = $junc2-$trst2+1; 
						$location = "intron";
						last;
					}
				}

			}
			if($arr[7] && $arr[8]){
				if(($arr[7]<=$junc2) and ($arr[8]>=$junc2)){
					$e=1;

					$location = "5'utr";
					$exst2=$arr[7];
					$exnd2=$arr[8];
				}
			}
			if($arr[9] && $arr[10]){
				if(($arr[9]<=$junc2) and ($arr[10]>=$junc2)){
					$e=1;

					$location = "3'utr";
					$arr[10] =~ s/\R|\n//g;
					$exst2=$arr[9];
					$exnd2=$arr[10];
				}
			}
			
			if($filter =~ /I/){
				if($location eq "intron"){
					pop @trst2;
					pop @trnd2;
					next;
				}
			}

			push @location2, $location;

			if($exst[0]>$trst2){
				$exon_junc_len += $exst[0]-$trst2+1;
			}
			if($exnd[@exnd-1]<$trnd2){
				$exon_junc_len += $trnd2-$exnd[@exnd-1]+1;
			}

			push @regst2, $exst2;
			push @regnd2, $exnd2;


			if($filter !~ /I/){
				my $s2_return;
				if($e){
					$s2_return = sequence_extractor::cdna_sequencing($str2, $ref_dir, $ensembl_ver, $trans_id, $exon_junc_len, "3", $str1.$str2, \%seqs);
					if (length($s2_return) >= 15 and $s2_return ne "no sequence"){
						push @seq_pass2, 1;
					}
					else{
						push @seq_pass2, 0;
					}
				}
				else{
					my $s2_intron = sequence_extractor::intron_region_sequencing($mapsplice_fa, $junc2, $str2, $chr2, "3", $intron_junc_len);
					my $s2_cdna = sequence_extractor::cdna_sequencing($str2, $ref_dir, $ensembl_ver, $trans_id, $exon_junc_len, "3", $str1.$str2, \%seqs);
					if($s2_cdna eq "no sequence"){
						$s2_cdna = "";
					}
					$s2_return = $s2_intron.$s2_cdna;
					if (length($s2_return) >= 15){
						push @seq_pass2, 1;				
					}
					else{
						push @seq_pass2, 0;
					}
				}
				push @trans2, $trans_id;
				push @e2, $e;
				push @s2, $s2_return;
			}
			elsif($e){
				my $s2_return = sequence_extractor::cdna_sequencing($str2, $ref_dir, $ensembl_ver, $trans_id, $exon_junc_len, "3", $str1.$str2, \%seqs);
				if (length($s2_return) >= 15 and $s2_return ne "no sequence"){
					push @seq_pass2, 1;
				}
				else{
					push @seq_pass2, 0;
				}
				push @trans2, $trans_id;
				push @e2, $e;
				push @s2, $s2_return;					
			}
		}
	}
	close(fileHandler4);

	if($find == 0){
		print "Warning!!! There is no Ensembl exon coordinates for gene ".$gene2.". Skipped.\n";
		return 0;
	}

#=cut
	my $as1=0;
	my $as2=0;
	my $seq;
	$find = 0;
	for($as1=0; $as1<@trans1; $as1++){
		for($as2=0; $as2<@trans2; $as2++){	
			my $region1;
			my $region2;
			my $splicing="Alternative splicing site";
			
			if($e1[$as1] == 1){
				if($junc1 == $regst1[$as1] or $junc1 == $regnd1[$as1]){
					$region1 = "exon_boundary";
				}
				else{
					$region1 = "exon";
				}
			}
			else{
				$region1 = "intron";
			}
			if($e2[$as2] == 1){
				if($junc2 == $regst2[$as2] or $junc2 == $regnd2[$as2]){
					$region2 = "exon_boundary";
				}
				else{
					$region2 = "exon";
				}
			}
			else{
				$region2 = "intron";
			}

			if($region1 eq "exon_boundary" and $region2 eq "exon_boundary"){
				$splicing = "Canonical splicing site";
			}
			
			if(($e1[$as1]>0 and $e2[$as2]>0) or ($filter !~ /I/)){
				$find = 1;
				print fileWriter2 $sample."\t".$chr1."\t".$chr2."\t".$gene1."\t".$trans1[$as1]."\t".$trst1[$as1]."\t".$trnd1[$as1]."\t".$gene2."\t".$trans2[$as2]."\t".$trst2[$as2]."\t".$trnd2[$as2]."\t".$str1."\t".$str2."\t".$location1[$as1]."\t".$regst1[$as1]."\t".$junc1."\t".$regnd1[$as1]."\t".$location2[$as2]."\t".$regst2[$as2]."\t".$junc2."\t".$regnd2[$as2]."\t".$region1."\t".$region2."\t".$splicing."\t".$anno."\t".$span_read."\t".$span_pair."\t".$program."\t";
			

				if($seq_pass1[$as1] and $seq_pass2[$as2]){
					print fileWriter2 "ok\n";
					$seq = $s1[$as1]."|".$s2[$as2];
					print fileWriter1 ">".$program."|".$gene1."(".$trans1[$as1]."):".$gene2."(".$trans2[$as2].") chr".$chr1.":".$junc1."(".$location1[$as1].")-chr".$chr2.":".$junc2."(".$location2[$as2].")_".int(index($seq,"|")+1)."\n";
					print fileWriter1 substr($seq,0,index($seq,"|")).substr($seq,index($seq,"|")+1)."\n";

					#print fileWriter4 ">".$program."|".$gene1.":".$gene2." chr".$chr1.":".$junc1."-chr".$chr2.":".$junc2."\n";
					#print fileWriter4 substr($seq,index($seq,"|")-15,15).substr($seq,index($seq,"|")+1,15)."\n";
					
				}
				else{
					print fileWriter2 "none\n";
				}
			}
		}
	}

	return $find;
	#$break1 = breakpoint_sequence_extractor::breakpoint_sequencing($mapsplice_fa, $junc1, $str1, $chr1, "5");
	#$break2 = breakpoint_sequence_extractor::breakpoint_sequencing($mapsplice_fa, $junc2, $str2, $chr2, "3");
	#print fileWriter5 ">".$gene1."(".$junc1.",".$str1.")\n".$break1."\n";
	#print fileWriter6 ">".$gene2."(".$junc2.",".$str2.")\n".$break2."\n";
}
1;


Ensembl_paralog();
Gencode_pseudogene();
gene_expression();
transcript_expression();
sequence_hash();

my $result = "merged.tmp";	#SOAPfuse + MapSplice2S + TopHat-fusion2S Results
open (fileWriter1, ">".$out_dir."/redundant_fusion_cDNA_".$filter.".fasta") || die "Cannot make new file1\n";
open (fileWriter2, ">".$out_dir."/FusionPro-merged_result_".$filter.".tsv") || die "Cannot make new file2\n";
open (fileWriter3, ">".$out_dir."/FusionPro-filtered_out_".$filter.".tsv") || die "Cannot make new file3\n";
open (fileHandler1, $result) || die "Cannot open ".$result.".\n";

my $i="";
my $line="";
my $seq="";
my $str1="";
my $str2="";
my $chr1="";
my $chr2="";
my $gene1="";
my $gene2="";
my $junc1="";
my $junc2="";
my $span_read=0;
my $span_pair=0;
my $program="";

#Processing result

while($line=<fileHandler1>){
	$line =~ s/\R|\n//g;
	unless($line eq ""){
		my @array = split /\t/,$line;

		$gene1=$array[0];
		$gene2=$array[1];
		$chr1=$array[2];
		$chr2=$array[3];
		$str1=$array[4];
		$str2=$array[5];
		$junc1=$array[6];
		$junc2=$array[7];
		$span_read=$array[8];
		$span_pair=$array[9];
		$program="";
		if($array[10] =~ /S/){
			$program .= "SOAPfuse,";
		}
		if($array[10] =~ /M/){
			$program .= "MapSplice2S,";
		}
		if($array[10] =~ /T/){
			$program .= "TopHat2S,";
		}
		chop($program);
		
		#annotating the fusion
		my $anno = Mitelman_annotation($gene1,$gene2);

		
		#transcript-level analysis
		my $filtered = fusion_analysis($gene1, $gene2, $chr1, $chr2, $str1, $str2, $junc1, $junc2, $span_read, $span_pair, $anno, $program);
		if($filtered == 0){
			print fileWriter3 $chr1."\t".$chr2."\t".$gene1."\t".$gene2."\t".$junc1."\t".$junc2."\t".$str1."\t".$str2."\n";
		}
	}
	$i++;
}


close(fileWriter1);
close(fileWriter2);
close(fileWriter3);
close(fileHandler1);


unlink($result);
