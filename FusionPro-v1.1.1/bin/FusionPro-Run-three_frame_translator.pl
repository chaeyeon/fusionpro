#!/usr/bin/perl
#translator.pl
use strict; 
use warnings;

my $fileName = $ARGV[2]."/".$ARGV[0];
my $filter = $ARGV[1];
my $enzyme = $ARGV[4];

open (fileHandle, $fileName) || die "Cannot open ".$fileName.".\n";
system("perl -I ".$ARGV[3]." ".$ARGV[3]."/FusionPro-Run-translate.pl -f 1 -b ".$ARGV[3]." -o ".$ARGV[2]." -p ".$filter." -e ".$enzyme." <$fileName> ".$ARGV[2]."/translation_1.fasta");
system("perl -I ".$ARGV[3]." ".$ARGV[3]."/FusionPro-Run-translate.pl -f 2 -b ".$ARGV[3]." -o ".$ARGV[2]." -p ".$filter." -e ".$enzyme." <$fileName> ".$ARGV[2]."/translation_2.fasta");
system("perl -I ".$ARGV[3]." ".$ARGV[3]."/FusionPro-Run-translate.pl -f 3 -b ".$ARGV[3]." -o ".$ARGV[2]." -p ".$filter." -e ".$enzyme." <$fileName> ".$ARGV[2]."/translation_3.fasta");
close(fileHandle);

unlink($ARGV[2]."/redundant_fusion_proteoform_".$filter.".fasta");
my $frame;
for ($frame=1;$frame<4 ;$frame++) {
	open (fileHandle, $ARGV[2]."/translation_".$frame.".fasta") || die "Cannot open translation_".$frame.".fasta.\n";
	open (fileWriter, ">>".$ARGV[2]."/redundant_fusion_proteoform_".$filter.".fasta") || die "Cannot make new file\n";
	my @allLines = <fileHandle>;
	print fileWriter @allLines;
	close(fileHandle);
	close(fileWriter);
	unlink $ARGV[2]."/translation_".$frame.".fasta";
}
