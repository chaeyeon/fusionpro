# -*- coding: utf-8 -*-
"""----------------------------------------"""
"""     title		: FusionPro	(v1.0.0)     """  
"""     made by		: ChaeYeon, Kim			 """
"""----------------------------------------"""	

import sys,os,shutil,copy,time,datetime,re
import configreader
import argparse

def ensure_file(f):
	if not os.path.isfile(f):
		print "[%s] err!!! No such file \("%(GetTime())+f+"\)"
		sys.exit()

def ensure_dir(d):
	if not os.path.isdir(d):
		print "[%s] err! No such directory ("%(GetTime())+d+")"
		sys.exit()

def remove_file(f):
	if os.path.isfile(f) :
		os.remove(f)
def remove_link(l):
	if os.path.islink(l) :
		os.unlink(l) 		

def GetTime():
    ts = time.time()
    return datetime.datetime.fromtimestamp(ts).strftime("%Y-%m-%d %H:%M:%S")
def WriteLog(log,l) :
	if not os.path.isdir(log[0:-14]):
		os.mkdir(log[0:-14])
	if not os.path.isfile(log):
		f = open(log,"w")
		f.write(l+"\n")
		f.close()
	else:
		f = open(log,'a')
		f.write(l+"\n")
		f.close()
	print l
def RunCommand(run,l) :
	if not os.path.isdir(run[0:-8]):
		os.mkdir(run[0:-8])
	if not os.path.isfile(run):
		f = open(run,"w")
		f.write(">"+l+"\n")
		f.close()
	else:
		f = open(run,'a')
		f.write(">"+l+"\n")
		f.close()
	os.system(l)
	




if __name__ == "__main__":
# command argusments
	parser = argparse.ArgumentParser(prog="FusionPro",usage='use "python FusionPro.py --help" for more information', description="Run FusionPro for fusion proteoform discovery.", formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument("-p", type=int, default=1, help="specify number of processor to use (default: %(default)s)")
	parser.add_argument("-c", type=file, default="Not specified", help="specify config file path",required=True)
	parser.add_argument("-m", default="RunPipeline", required=True, \
	help="""\
specify module to run (default: %(default)s)
  - CreateReference
  - RunPipeline
  - RunSoapfuse
  - RunTophatfusion(S)
  - RunMapsplice(S)
  - RunCufflinks
  - ProcessResults
  - FindFusionProteoform
  - MergeMultipleResults""")
	parser.add_argument("-qs", default="phred33", \
	help="""\
specify quality-scale
  - Illumina 1.8+, Sanger	-> phred33
  - Illumina 1.3+ ~ 1.7+	-> phred64
  - Solexa64		-> solex64
(default: %(default)s)""")
	parser.add_argument("-f", default="IPX", \
	help="""\
choose filtering options
  - Intron filtration			-> I
  - Paralog filtration			-> P
  - Expression filtration (Cufflinks)	-> X
  - No filtration				-> N
(default: %(default)s)""")
	parser.add_argument("-o", help="specify output directory absolute path", required=True)
	parser.add_argument("-1", "--fq", default="Not specified", help="specify fastq file 1 path")
	parser.add_argument("-2", "--fq2", default="Not specified", help="specify fastq file 2 path")
	


	args = parser.parse_args()

	ensure_file(args.c.name)

	p1 = re.compile("[ABCDEFGHJKLNMOQRSTUVWYZabcdefghjklmmoqrstuvwyz]+")
	p4 = re.compile(".{4,}");
	p5 = re.compile("^[Nn]$")
	m1 = p1.search(args.f)
	m4 = p4.match(args.f)
	m5 = p5.match(args.f)
	if m1 and not m5:
		print "[%s] err! No such option \""%(GetTime()) + args.f +"\""
		sys.exit()
	elif m4:
		print "[%s] err! Too long option \""%(GetTime()) + args.f +"\""
		sys.exit()


# Config values
	cfg = configreader.reader(args.c)

	ensembl_genome_version	= cfg.get_value("ensembl_genome_version").rstrip()
	ensembl_version			= cfg.get_value("ensembl_version").rstrip()
	ucsc_version			= cfg.get_value("ucsc_genome_version").rstrip()
	read_length				= cfg.get_value("read_length").rstrip()
	source_directory		= cfg.get_value("source_directory").rstrip()
	dataset_directory		= cfg.get_value("db_dir").rstrip()
	bowtie_build_bin		= cfg.get_value("bowtie_build_bin").rstrip()
	tophat2_bin				= cfg.get_value("tophat2_bin").rstrip()
	tophat_fusion_bin		= cfg.get_value("tophat_fusion_bin").rstrip()
	cufflinks_bin			= cfg.get_value("cufflinks_bin").rstrip()
	soapfuse_dir			= cfg.get_value("soapfuse_dir").rstrip()
	mapsplice_bin			= cfg.get_value("mapsplice_bin").rstrip()
	oncofuse_bin			= cfg.get_value("oncofuse_bin").rstrip()
	ref_genome_fasta		= dataset_directory+"/Homo_sapiens."+ensembl_genome_version+"."+ensembl_version+".dna.chromosomes.fa"
	ref_genome_model		= dataset_directory+"/Homo_sapiens."+ensembl_genome_version+"."+ensembl_version+".gtf"
	mapsplice_ref_dir		= cfg.get_value("mapsplice_ref_dir").rstrip()
	soapfuse_source_dir		= cfg.get_value("ps_dir").rstrip()

	abundance_cutoff		= cfg.get_value("abundance_cutoff").rstrip()
	enzyme					= cfg.get_value("enzyme").rstrip()


	#tophat_option
	mate_inner_dist			= cfg.get_value("mate_inner_dist").rstrip()
	mate_std_dev			= cfg.get_value("mate_std_dev").rstrip()
	gtf						= cfg.get_value("tophat_gtf").rstrip()


	os.chdir(source_directory)
	#output directory creation
	if not os.path.isdir(args.o):
		os.mkdir(args.o)
	#referece directory creation
	if not os.path.isdir(dataset_directory):
		os.mkdir(dataset_directory)


	log = args.o+"/logs/FusionPro.log"
	run = args.o+"/logs/run.log"



		
	mod = 0 
	filter = ''.join(sorted(args.f))
	filter = filter.upper()

	#create reference dataset
	if (args.m == "CreateReference"):
		mod = 1
		WriteLog(log, "[%s] Creating all reference dataset..." % (GetTime()))

		if os.path.isfile("./FusionPro-Lib-common_ref_builder.pl"):
			RunCommand(run, "perl FusionPro-Lib-common_ref_builder.pl %s %s %s %s %s" \
			%(ensembl_genome_version, ensembl_version, ref_genome_fasta, ref_genome_model, dataset_directory))

			RunCommand(run,"perl %s/FusionPro-Lib-chr_changer.pl %s" % (source_directory, ref_genome_fasta))
			remove_file(ref_genome_fasta)
			shutil.move(ref_genome_fasta[:-3]+"_.fa", ref_genome_fasta)

			# Create SOAPfuse-specific reference dataset
			os.chdir(soapfuse_source_dir)

			# Prepare Soapfuse-specific reference dataset (HGNC gene family dataset and cytoband database)
			if os.path.isfile("cytoBand.txt"):
				remove_file("cytoBand.txt")
			shutil.copy(source_directory+"/../library/cytoBand.txt","./cytoBand.txt")
			if os.path.isfile("HGNC_gene_family.tsv"):
				remove_file("HGNC_gene_family.tsv")
			shutil.copy(source_directory+"/../library/HGNC_gene_family.tsv","./HGNC_gene_family.tsv")
			if os.path.isfile("rft_list.txt"):
				remove_file("rft_list.txt")
			shutil.copy(source_directory+"/../library/rft_list.txt","./rft_list.txt")


			if os.path.isfile("SOAPfuse-S00-Generate_SOAPfuse_database.pl") :
				RunCommand(run,"perl SOAPfuse-S00-Generate_SOAPfuse_database.pl -wg %s_soapfuse.fa -gtf %s -cbd cytoBand.txt -gf HGNC_gene_family.tsv -rft rft_list.txt -sd %s -dd %s" \
				%(ref_genome_fasta[:-3], ref_genome_model, soapfuse_dir, dataset_directory))
			else :	
				WriteLog(log, "[%s] err!!! Please install SOAPfuse or check SOAPfuse source directory path.!" % (GetTime()))
				sys.exit()
			
			os.chdir(dataset_directory)
			
			# Create MapSplice2-specific reference dataset (genome fa of each chromosome)
			for file in os.listdir(os.getcwd()):
				if file.startswith("dna.chromosome."):
					os.rename(file,file[15:])
					if not os.path.isdir(mapsplice_ref_dir):
						os.mkdir(mapsplice_ref_dir)
					remove_file(mapsplice_ref_dir+"/"+file[15:])
					shutil.move(os.getcwd()+"/"+file[15:], mapsplice_ref_dir)

			os.chdir(source_directory)

			WriteLog(log, "[%s] Reference dataset download is completed." % (GetTime()))

		else : 
			WriteLog(log, "[%s] err!!! Please install all programs or check config file.!" % (GetTime()))
			sys.exit()



	#Building Bowtie index 
	if (args.m == "CreateReference" or args.m == "RunPipeline"):
		mod = 1
		WriteLog(log, "[%s] Building Bowtie index..." % (GetTime()))
		if os.path.isfile(bowtie_build_bin) : 
			if os.path.isfile(ref_genome_fasta[:-3]+".1.ebwt") : 
				WriteLog(log,"[%s] Bowtie index exists! Skip this step." % (GetTime()))
			else :	
				print "a";
				RunCommand(run, "%s %s %s" %(bowtie_build_bin, ref_genome_fasta, ref_genome_fasta[:-3]))
				WriteLog(log, "[%s] Building Bowtie index is completed." % (GetTime()))
		else :
			WriteLog(log, "err!!! Please install Bowtie-build or check config file.!")
			sys.exit()


	#SOAPfuse
	if (args.m == "RunPipeline" or args.m == "RunSoapfuse"):
		mod = 1
		ensure_file(args.fq)
		ensure_file(args.fq2)
		WriteLog(log, "[%s] Searching gene fusions using SOAPfuse..." % (GetTime()))
		
		fq_dir = args.fq.split('/')
		sample_id = fq_dir[-3]
		seq_lib_id = fq_dir[-2]
		run_id= fq_dir[-1][:re.search(r"_[1|2].f[astq|q]", fq_dir[-1]).start()]
		fq_path = ""
		for e in fq_dir[:-3]:
			fq_path += "/"+e

		# Create SOAPfuse-specific reference dataset
		os.chdir(soapfuse_source_dir)

		# Prepare Soapfuse-specific reference dataset (HGNC gene family dataset and cytoband database)
		if os.path.isfile("cytoBand.txt"):
			remove_file("cytoBand.txt")
		shutil.copy(source_directory+"/../library/cytoBand.txt","./cytoBand.txt")
		if os.path.isfile("HGNC_gene_family.tsv"):
			remove_file("HGNC_gene_family.tsv")
		shutil.copy(source_directory+"/../library/HGNC_gene_family.tsv","./HGNC_gene_family.tsv")
		if os.path.isfile("rft_list.txt"):
			remove_file("rft_list.txt")
		shutil.copy(source_directory+"/../library/rft_list.txt","./rft_list.txt")

		os.chdir(args.o)

		soap_f = open("sample_list.txt", 'w')
		soap_f.write(sample_id+"\t"+seq_lib_id+"\t"+run_id+"\t"+read_length)
		soap_f.close()
	

		os.chdir(source_directory)

		if os.path.isfile(soapfuse_dir+"/SOAPfuse-RUN.pl"):
			RunCommand(run, "perl %s/SOAPfuse-RUN.pl -o %s/soapfuse_out -c %s -fd %s -l %s" % (soapfuse_dir, args.o, args.c.name, fq_path[1:], args.o+"/sample_list.txt"))
			if os.path.isfile(args.o+"/soapfuse_out/final_fusion_genes/"+sample_id+"/"+sample_id+".final.Fusion.specific.for.genes"):
				WriteLog(log, "[%s] SOAPfuse successfully finished running." % (GetTime()))
			else:
				WriteLog(log, "[%s] SOAPfuse finished with error." % (GetTime()))
		else :
			WriteLog(log, "[%s] err!!! Please install SOAPfuse or check config file.!" % (GetTime()))
			sys.exit()

	#Process SOAPfuse results
	if (args.m == "RunPipeline" or args.m == "RunSoapfuse" or args.m == "RunMapspliceS" or args.m == "RunTophatfusionS"):
		with open(args.o+"/sample_list.txt", 'r') as content_file:
			content = content_file.read()
		s_list = content.split()

		WriteLog(log, "[%s] Processing intermediate results of SOAPfuse..." % (GetTime()))	
		RunCommand(run, "perl FusionPro-Run-SOAPfuse_unmapped2fasta.pl "+args.o+"/soapfuse_out/align_unmap_Tran/"+s_list[0]+"/"+s_list[1]+"/"+s_list[2]+".WG.Tran.unmap.gz "+args.o+"/soapfuse_out/unmapped")


	#Soapfuse -> MapSplice
	if (args.m == "RunPipeline" or args.m == "RunMapspliceS"):
		mod = 1
		soap_unmap = args.o+"/soapfuse_out/unmapped.fasta"
		ensure_file(soap_unmap)
		WriteLog(log, "[%s] Searching gene fusions using MapSplice2..." % (GetTime()))
		#mapsplices option ���� (--qual-scale)
		if os.path.isfile(mapsplice_bin):
			RunCommand(run, "python %s -o %s/mapsplice_out -p %d  --fusion --bam --qual-scale %s --gene-gtf %s -c %s -x %s -1 %s"\
			% (mapsplice_bin, args.o, args.p, args.qs, ref_genome_model, mapsplice_ref_dir, ref_genome_fasta[:-3], soap_unmap))
			if os.path.isfile(args.o+"/mapsplice_out/fusions_well_annotated.txt"):
				WriteLog(log, "[%s] MapSplice2S successfully finished running." % (GetTime()))
			else:
				WriteLog(log, "[%s] MapSplice2S finished with error." % (GetTime()))
			
		else :
			WriteLog(log, "[%s] err!!! Please install MapSplice2 or check config file.!" % (GetTime()))
			sys.exit()



	#Soapfuse -> Tophat
	if (args.m == "RunPipeline" or args.m == "RunTophatfusionS"):
		mod = 1
		with open(args.o+"/sample_list.txt", 'r') as content_file:
			content = content_file.read()
		s_list = content.split()
		
		soap_disc1 = args.o+"/soapfuse_out/alignWG/"+s_list[0]+"/"+s_list[1]+"/"+s_list[2]+".WG.disc.for-trans-align_1.fq.gz"
		soap_disc2 = args.o+"/soapfuse_out/alignWG/"+s_list[0]+"/"+s_list[1]+"/"+s_list[2]+".WG.disc.for-trans-align_2.fq.gz"
		soap_unmap = args.o+"/soapfuse_out/unmapped.fasta"

		ensure_file(soap_disc1)
		ensure_file(soap_disc2)
		ensure_file(soap_unmap)

		WriteLog(log, "[%s] Searching gene fusions using TopHat2..." % (GetTime()))		
		if os.path.isfile(tophat2_bin):
			#os.rename(ref_genome_fasta,ref_genome_fasta+".fa")
			if(gtf == "yes"):
				RunCommand(run, tophat2_bin+" -o %s/tophat_out -p %d --fusion-search --no-coverage-search --keep-fasta-order --bowtie1 -G %s -r %d --mate-std-dev %d --max-intron-length 100000 --fusion-min-dist 100000 --fusion-anchor-length 13 --fusion-ignore-chromosomes MT --library-type fr-unstranded %s %s %s,%s"\
				%(args.o, args.p, ref_genome_model, int(mate_inner_dist), int(mate_std_dev), ref_genome_fasta[:-3], soap_disc1, soap_disc2, soap_unmap))
			elif(gtf == "no"):
				RunCommand(run, tophat2_bin+" -o %s/tophat_out -p %d --fusion-search --no-coverage-search --keep-fasta-order --bowtie1 -r %d --mate-std-dev %d --max-intron-length 100000 --fusion-min-dist 100000 --fusion-anchor-length 13 --fusion-ignore-chromosomes MT --library-type fr-unstranded %s %s %s,%s"\
				%(args.o, args.p, int(mate_inner_dist), int(mate_std_dev), ref_genome_fasta[:-3], soap_disc1, soap_disc2, soap_unmap))
			#os.rename(ref_genome_fasta+".fa", ref_genome_fasta)

			if os.path.isfile(args.o+"/tophat_out/accepted_hits.bam" and args.o+"/tophat_out/fusions.out"):
				WriteLog(log, "[%s] TopHat2S successfully finished running." % (GetTime()))
			else:
				WriteLog(log, "[%s] TopHat2S finished with error." % (GetTime()))

		else : 
			WriteLog(log, "[%s] err!!! Please install TopHat2 or check config file.!" % (GetTime()))
			sys.exit()



	#MapSplice
	if (args.m == "RunMapsplice"):
		mod = 1
		ensure_file(args.fq)
		ensure_file(args.fq2)
		WriteLog(log, "[%s] Searching gene fusions using MapSplice2..." % (GetTime()))
		#mapsplices option ���� (--qual-scale)
		if os.path.isfile(mapsplice_bin):
			RunCommand(run, "python %s -o %s/mapsplice_out -p %d  --fusion-non-canonical --bam --qual-scale %s --gene-gtf %s -c %s -x %s -1 %s -2 %s"\
			% (mapsplice_bin, args.o, args.p, args.qs, ref_genome_model, mapsplice_ref_dir, ref_genome_fasta[:-3], args.fq, args.fq2))
			
			if os.path.isfile(args.o+"/mapsplice_out/fusions_well_annotated.txt"):
				WriteLog(log, "[%s] MapSplice2 successfully finished running." % (GetTime()))
			else:
				WriteLog(log, "[%s] MapSplice2 finished with error." % (GetTime()))
		else :
			WriteLog(log, "[%s] err!!! Please install MapSplice or check config file.!" % (GetTime()))
			sys.exit()



	#Tophat/Bowtie
	if (args.m == "RunTophatfusion"):
		mod = 1
		ensure_file(args.fq)
		ensure_file(args.fq2)
		WriteLog(log, "[%s] Searching gene fusions using TopHat2..." % (GetTime()))		
		if os.path.isfile(tophat2_bin):
			#os.rename(ref_genome_fasta,ref_genome_fasta+".fa")
			if(gtf == "yes"):
				RunCommand(run, tophat2_bin+" -o %s/tophat_out -p %d --fusion-search --no-coverage-search --keep-fasta-order --bowtie1 -G %s -r %d --mate-std-dev %d --max-intron-length 100000 --fusion-min-dist 100000 --fusion-anchor-length 13 --fusion-ignore-chromosomes MT --library-type fr-unstranded %s %s %s"\
				%(args.o, args.p, ref_genome_model, int(mate_inner_dist), int(mate_std_dev), ref_genome_fasta[:-3], args.fq, args.fq2))
			elif(gtf == "no"):
				RunCommand(run, tophat2_bin+" -o %s/tophat_out -p %d --fusion-search --no-coverage-search --keep-fasta-order --bowtie1 -r %d --mate-std-dev %d --max-intron-length 100000 --fusion-min-dist 100000 --fusion-anchor-length 13 --fusion-ignore-chromosomes MT --library-type fr-unstranded %s %s %s"\
				%(args.o, args.p, int(mate_inner_dist), int(mate_std_dev), ref_genome_fasta[:-3], args.fq, args.fq2))
			#os.rename(ref_genome_fasta+".fa", ref_genome_fasta)

			if os.path.isfile(args.o+"/tophat_out/accepted_hits.bam" and args.o+"/tophat_out/fusions.out"):
				WriteLog(log, "[%s] TopHat2 successfully finished running." % (GetTime()))
			else:
				WriteLog(log, "[%s] TopHat2 finished with error." % (GetTime()))

		else : 
			WriteLog(log, "[%s] err!!! Please install TopHat2 or check config file.!" % (GetTime()))
			sys.exit()
	if (args.m == "RunPipeline" or args.m == "RunTophat-fusion-post" or args.m == "RunTophatfusion" or args.m == "RunTophatfusionS"):
		mod = 1
		#tophat-fusion-post
		if os.path.isfile(tophat_fusion_bin):
			cur_dir = os.getcwd()
			WriteLog(log, "[%s] Searching gene fusions using TopHat-fusion-post..." % (GetTime()))	
			os.chdir(args.o)
			shutil.copy(cur_dir+"/../library/refGene_"+ucsc_version+".txt",args.o+"/refGene.txt")
			shutil.copy(cur_dir+"/../library/ensGene_"+ucsc_version+".txt",args.o+"/ensGene.txt")
			RunCommand(run, tophat_fusion_bin+" -p %d --num-fusion-reads 3 --num-fusion-pairs 2 --num-fusion-both 5 --skip-blast %s"\
			% (args.p, ref_genome_fasta[:-3]))
			os.chdir(cur_dir)
			if os.path.isfile(args.o+"/tophatfusion_out/potential_fusion.txt" and args.o+"/tophatfusion_out/result.txt"):
				WriteLog(log, "[%s] TopHat-fusion-post successfully finished running." % (GetTime()))
			else:
				WriteLog(log, "[%s] TopHat-fusion-post finished with error." % (GetTime()))
		else :
			WriteLog(log, "[%s] err!!! Please install TopHat-fusion-post or check config file.!" % (GetTime()))
			sys.exit()

	if (args.m == "RunPipeline" or args.m == "RunCufflinks"):
		mod = 1
		#cufflinks
		WriteLog(log, "[%s] Analyzing gene/transcript expression level with Cufflinks..." % (GetTime()))	
		if os.path.isfile(cufflinks_bin):
			RunCommand(run, cufflinks_bin+" -o %s/cufflinks_out -p %d -G %s %s" % (args.o, args.p, ref_genome_model, args.o+"/tophat_out/accepted_hits.bam"))



	#Filtering and Merging
	if (args.m == "RunPipeline" or args.m == "ProcessResults"):
		mod = 1

		with open(args.o+"/sample_list.txt", 'r') as content_file:
			content = content_file.read()
		s_list = content.split()
	
		WriteLog(log, "[%s] Filtering and merging the results...(option: %s)" % (GetTime(),filter))
		if not (os.path.isfile(args.o+"/soapfuse_out/final_fusion_genes/"+s_list[0]+"/"+s_list[0]+".final.Fusion.specific.for.genes") and os.path.isfile(args.o+"/tophatfusion_out/result.txt") and os.path.isfile(args.o+"/tophatfusion_out/potential_fusion.txt") and os.path.isfile(args.o+"/mapsplice_out/fusions_well_annotated.txt")):
			WriteLog(log, "[%s] err!!! Please check the result files exist!" % (GetTime()))
			sys.exit()
			if not os.path.isfile(args.o+"/soapfuse_out/final_fusion_genes/"+s_list[0]+"/"+s_list[0]+".final.Fusion.specific.for.genes") :
				f = open(args.o+"/soapfuse_out/final_fusion_genes/"+s_list[0]+"/"+s_list[0]+".final.Fusion.specific.for.genes","wt")
				f.close()
			if not os.path.isfile(args.o+"/tophatfusion_out/potential_fusion.txt") :
				f = open(args.o+"/tophatfusion_out/potential_fusion.txt","wt")
				f.close()
			if not os.path.isfile(args.o+"/mapsplice_out/fusions_well_annotated.txt") :
				f = open(args.o+"/mapsplice_out/fusions_well_annotated.txt","wt")
				f.close()
			
		if not os.path.isdir(args.o+"/combined_result"):
			os.mkdir(args.o+"/combined_result")

		shutil.copy(args.o+"/soapfuse_out/final_fusion_genes/"+s_list[0]+"/"+s_list[0]+".final.Fusion.specific.for.genes", args.o+"/combined_result/")
		shutil.copy(args.o+"/tophatfusion_out/potential_fusion.txt",args.o+"/combined_result/")
		shutil.copy(args.o+"/tophatfusion_out/result.txt",args.o+"/combined_result/")
		shutil.copy(args.o+"/mapsplice_out/fusions_well_annotated.txt",args.o+"/combined_result/")

		cur_dir = os.getcwd()
		os.chdir(args.o+"/combined_result/")
		out_dir = os.getcwd()
		
		if not os.path.isfile("exon_coord.tsv"):
			RunCommand(run, "perl "+cur_dir+"/FusionPro-Run-coord_generator.pl "+ref_genome_model+" "+out_dir+" "+ensembl_genome_version)
		
		RunCommand(run, "perl "+cur_dir+"/FusionPro-Run-fusion_result_merger.pl "+cur_dir+" "+out_dir+" "+s_list[0])
		RunCommand(run, "perl "+cur_dir+"/FusionPro-Run-fusion_result_processor.pl "+cur_dir+" "+dataset_directory+" "+ensembl_genome_version+"."+ensembl_version+" "+filter+" "+out_dir+" "+s_list[0]+" "+args.o+"/cufflinks_out "+s_list[2])
		RunCommand(run, "perl "+cur_dir+"/FusionPro-Run-summary_maker.pl "+cur_dir+" "+filter+" "+out_dir)
		RunCommand(run, "perl "+cur_dir+"/FusionPro-Run-redundant_remover.pl redundant_fusion_cDNA_"+filter+".fasta "+filter+" "+out_dir)
		
		WriteLog(log, "[%s] Oncofuse searching..." % (GetTime()))
		RunCommand(run, "perl "+cur_dir+"/FusionPro-Run-oncofuse_formatter.pl "+filter+" "+out_dir+" FusionPro-merged_result")
		RunCommand(run, "java -Xmx1G -jar "+oncofuse_bin+" -a "+ucsc_version+" oncofuse_coord_"+filter+".txt coord - oncofuse_out_"+filter+".txt")
		RunCommand(run, "perl "+cur_dir+"/FusionPro-Run-driver_predictor.pl "+filter+" "+out_dir)
		WriteLog(log, "[%s] Complete filtering and merging steps..." % (GetTime()))

		os.chdir(cur_dir)



	if (args.m == "RunPipeline" or args.m == "FindFusionProteoform"):
		mod = 1
		
		WriteLog(log, "[%s] 3-frame translating..." % (GetTime()))
		
		cur_dir = os.getcwd()
		os.chdir(args.o+"/combined_result/")
		out_dir = os.getcwd()
		RunCommand(run, "perl "+cur_dir+"/FusionPro-Run-three_frame_translator.pl FusionPro-final_fusion_cDNA_"+filter+".fasta "+filter+" "+out_dir+" "+cur_dir+" "+enzyme)
		RunCommand(run, "perl "+cur_dir+"/FusionPro-Run-redundant_remover_2.pl "+filter+" "+out_dir+" redundant_fusion_proteoform FusionPro-final_fusion_proteoform")
		RunCommand(run, "perl "+cur_dir+"/FusionPro-Run-redundant_remover_2.pl "+filter+" "+out_dir+" redundant_fusion_proteotypic_peptide FusionPro-final_fusion_proteotypic_peptide")
		remove_file(out_dir+"/redundant_fusion_proteotypic_peptide_"+filter+".fasta")
		WriteLog(log, "[%s] Complete translating steps..." % (GetTime()))
		os.chdir(cur_dir)

	if (args.m == "MergeMultipleResults"):
		mod = 1
		sample = raw_input ("Input sample name: ")
		#output directory creation
		merged = args.o+"/Merged_output_"+sample
		if not os.path.isdir(merged):
			os.mkdir(merged)
		
		log = merged+"/"+"logs/FusionPro.log"
		run = merged+"/"+"logs/run.log"
		WriteLog(log, "[%s] Merging " % (GetTime()) +sample+" sample results...")
		
		cur_dir = os.getcwd()
		os.chdir(args.o)
		i=1
		for root, dirs, files in os.walk('./'):
			for dir in dirs:
				p = re.compile(r'.*'+sample+r'.*')
				m = p.match(dir)
				if m:
					if os.path.isfile(os.getcwd()+"/"+dir+"/combined_result/FusionPro-final_fusion_cDNA_"+filter+".fasta") and os.path.isfile(os.getcwd()+"/"+dir+"/combined_result/FusionPro-merged_result_"+filter+".tsv"):
						shutil.copy(os.getcwd()+"/"+dir+"/combined_result/FusionPro-final_fusion_cDNA_"+filter+".fasta",merged)
						os.rename(merged+"/FusionPro-final_fusion_cDNA_"+filter+".fasta",merged+"/FusionPro-final_fusion_cDNA_"+filter+"_"+str(i)+".fasta")
						shutil.copy(os.getcwd()+"/"+dir+"/combined_result/FusionPro-merged_result_"+filter+".tsv",merged)
						os.rename(merged+"/FusionPro-merged_result_"+filter+".tsv",merged+"/FusionPro-merged_result_"+filter+"_"+str(i)+".tsv")
						i=i+1
		os.chdir(merged)
		out_dir = os.getcwd()
		flist = os.listdir(merged)
		fout = open("merged_"+filter+".fasta", "w")
		for file in flist:
			p = re.compile('FusionPro-final_fusion_cDNA_'+filter+'_.*')
			m = p.match(file)
			if m:	
				f = open(file)
				content = f.read()
				fout.write(content)
				f.close()
				remove_file(file)
		fout.close()

		fout = open("FusionPro-merged_result_"+filter+".tsv", "w")
		for file in flist:
			p = re.compile('FusionPro-merged_result_'+filter+'_.*')
			m = p.match(file)
			if m:	
				f = open(file)
				content = f.read()
				fout.write(content)
				f.close()
				remove_file(file)
		fout.close()

		f = open("FusionPro-merged_result_"+filter+".tsv")
		
		RunCommand(run, "perl "+cur_dir+"/FusionPro-Run-summary_maker.pl "+cur_dir+" "+filter+" "+out_dir)
		RunCommand(run, "perl "+cur_dir+"/FusionPro-Run-redundant_remover.pl merged_"+filter+".fasta "+filter+" "+out_dir)	
		
		WriteLog(log, "[%s] 3-frame translating..." % (GetTime()))
		
		os.chdir(args.o+"/Merged_output_"+sample)
		out_dir = os.getcwd()
		RunCommand(run, "perl "+cur_dir+"/FusionPro-Run-three_frame_translator.pl FusionPro-final_fusion_cDNA_"+filter+".fasta "+filter+" "+out_dir+" "+cur_dir+" "+enzyme)
		RunCommand(run, "perl "+cur_dir+"/FusionPro-Run-redundant_remover_2.pl "+filter+" "+out_dir+" redundant_fusion_proteoform FusionPro-final_fusion_proteoform")
		RunCommand(run, "perl "+cur_dir+"/FusionPro-Run-redundant_remover_2.pl "+filter+" "+out_dir+" redundant_fusion_proteotypic_peptide FusionPro-final_fusion_proteotypic_peptide")
		remove_file(out_dir+"/redundant_fusion_proteotypic_peptide_"+filter+".fasta")
		WriteLog(log, "[%s] Complete translating steps..." % (GetTime()))
		os.chdir(cur_dir)

		WriteLog(log, "[%s] Oncofuse searching..." % (GetTime()))
		RunCommand(run, "perl "+cur_dir+"/FusionPro-Run-oncofuse_formatter.pl "+filter+" "+out_dir+" FusionPro-merged_result")
		RunCommand(run, "java -Xmx1G -jar "+oncofuse_bin+" -a "+ucsc_version+" "+out_dir+"/oncofuse_coord_"+filter+".txt coord - "+out_dir+"/oncofuse_out_"+filter+".txt")
		RunCommand(run, "perl "+cur_dir+"/FusionPro-Run-driver_predictor.pl "+filter+" "+out_dir)
		
		WriteLog(log, "[%s] Complete merging multiple samples..." % (GetTime()))

		os.chdir(cur_dir)


	if (mod == 0):
		print "[%s] err!!! No such module \""%(GetTime()) +args.m+"\"" 
		sys.exit()
