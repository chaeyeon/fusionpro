#!/usr/bin/perl
#sequence_extractor.pl
use strict;
use warnings;

package sequence_extractor;


#$_[0] = str
#$_[1] = reference library path
#$_[2] = ensembl_version
#$_[3] = transcript id 
#$_[4] = length to breakpoint 
#$_[5] = 5' or 3'
#$_[6] = sequence direction
sub cdna_sequencing {	
	my $str = $_[0]; 

	#sequence parsing
	my $s="";
	my $trans_id = $_[3];
	my $junc_len = int($_[4]);
	my $find = 0;
	my $l;
	my $dir = $_[6];

	open(fileHandle1,$_[1]."/Homo_sapiens.".$_[2].".cdna.ncrna.chromosomes.fa") || die("Cannot open Homo_sapiens.".$_[2].".cdna.ncrna.chromosomes.fa\n");
	while($l=<fileHandle1>){
		if($l =~ />/){
			if($find == 1){
				last;
			}

			my $id;
			my $frame="+";
			if($l =~ /(ENST[0-9]+)/){
				$id = $1;
			}
			if($l =~ /:([-]*1) gene/){
				if($1 eq "-1"){
					$frame = "-";
				}
				elsif($1 eq "1"){
					$frame = "+";
				}
			}
					
			if(($id eq $trans_id) and ($frame eq $str)){
				$find = 1;
			}
		}
		elsif($find == 1){
			chomp($l);
			$s = $s.$l;
		}
	}
	if($find and (length($s) > $junc_len) and ($junc_len != 0)){
		if($dir eq "++"){
			if($_[5] eq "5"){
				$s = substr($s,0,$junc_len);
			}
			elsif($_[5] eq "3"){
				$s = substr($s,$junc_len-1);
			}
		}
		elsif($dir eq "+-"){
			if($_[5] eq "5"){
				$s = substr($s,0,$junc_len);
			}
			elsif($_[5] eq "3"){
				$s = substr($s,length($s)-$junc_len);
			}	
		}
		elsif($dir eq "--"){
			if($_[5] eq "5"){
				$s = substr($s,0,length($s)-$junc_len+1);
			}
			elsif($_[5] eq "3"){
				$s = substr($s,length($s)-$junc_len);
			}
		}
		elsif($dir eq "-+"){
			if($_[5] eq "5"){
				$s = substr($s,0,length($s)-$junc_len);
			}
			elsif($_[5] eq "3"){
				$s = substr($s,$junc_len+1);
			}
		}
	}
	close(fileHandle1);
	if($s ne ""){
		return $s;
	}
	else{
		return "no sequence";
	}
}




#$_[0] = mapsplice_fa location
#$_[1] = junction
#$_[2] = sequence direction
#$_[3] = chromosome
#$_[4] = 5' or 3'
sub intron_region_sequencing {	
	my $str = $_[2];
	my $loc = $_[4];
	my $chr = $_[3];
	my $len = $_[5];
	my $exnd = $_[1]+$len;
	my $exst = $_[1]-$len;
	my $junc = $_[1];
		
	#sequence parsing
	my $s="";

	#gene1 sequence
	my $lcnt=-1;
	my $l;
	my @exst=(0,0);
	my @exnd=(0,0);
	if($str eq "+"){
		if($loc eq "5"){
			@exst = (int(($exst-1)/60),($exst-1)%60);
			@exnd = (int(($junc)/60),($junc)%60);
		}
		elsif($loc eq "3"){
			@exst = (int(($junc-1)/60),($junc-1)%60);
			@exnd = (int(($exnd)/60),($exnd)%60);
		}
	}
	elsif($str eq "-"){
		if($loc eq "5"){
			@exst = (int(($junc-1)/60),($junc-1)%60);
			@exnd = (int(($exnd)/60),($exnd)%60);
		}
		elsif($loc eq "3"){
			@exst = (int(($exst-1)/60),($exst-1)%60);
			@exnd = (int(($junc)/60),($junc)%60);
		}
	}

	open(fileHandle1,$_[0]."/".$chr.".fa") || die("Cannot open ".$_[0]."/".$chr.".fa\n");
	while($l=<fileHandle1>){
		if ($lcnt >= $exst[0]) {
			if($exst[0] == $exnd[0]){
				$s = substr($l,$exst[1],$exnd[1]-$exst[1]);
				chomp($s);
				last;
			}
			elsif($lcnt == $exst[0]){
				$s = substr($l,$exst[1]);
				chomp($s);
			}
			elsif($lcnt < $exnd[0]){
				$s .= $l;
				chomp($s);
			}
			elsif(($lcnt == $exnd[0])){
				$s .= substr($l,0,$exnd[1]);
				chomp($s);
				last;
			}
		}
		$lcnt++;
	}

	if($str eq "-"){
		$s=~tr/ATGCatgcNn/TACGtacgNn/;
		$s=reverse $s;
	}
	close(fileHandle1);
	return $s;
}

1;

