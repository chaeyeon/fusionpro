#!/usr/bin/perl
use strict;
use warnings;

package breakpoint_sequence_extractor;

#$_[0] = mapsplice_fa location
#$_[1] = junction
#$_[2] = sequence direction
#$_[3] = chromosome
#$_[4] = 5' or 3'
sub breakpoint_sequencing {	
	my $str = $_[2];
	my $loc = $_[4];
	my $chr = $_[3];
	my $junc = $_[1];
		
	#sequence parsing
	my $s="";

	#gene1 sequence
	my $lcnt=-1;
	my $l;
	my @exst=(0,0);
	my @exnd=(0,0);
	
	@exst = (int(($junc-25)/60),($junc-25)%60);
	@exnd = (int(($junc+25)/60),($junc+25)%60);
	
	open(fileHandle1,$_[0]."/".$chr.".fa") || die("Cannot open ".$_[0]."/".$chr.".fa\n");
	while($l=<fileHandle1>){
		if ($lcnt >= $exst[0]) {
			if($exst[0] == $exnd[0]){
				$s = substr($l,$exst[1],20);
				chomp($s);
				last;
			}
			elsif($lcnt == $exst[0]){
				$s = substr($l,$exst[1]);
				chomp($s);
			}
			elsif($lcnt < $exnd[0]){
				$s .= $l;
				chomp($s);
			}
			elsif(($lcnt == $exnd[0])){
				$s .= substr($l,0,$exnd[1]);
				chomp($s);
				last;
			}
		}
		$lcnt++;
	}

	if($str eq "-"){
		$s=~tr/ATGCatgcNn/TACGtacgNn/;
		$s=reverse $s;
	}
	return $s;
}

1;

