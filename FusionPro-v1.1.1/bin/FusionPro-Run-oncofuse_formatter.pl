use strict;
use warnings;

my $filter = $ARGV[0];
my $l;
open (fileWriter1, ">".$ARGV[1]."/oncofuse_coord_".$filter.".txt") || die "Cannot make new file\n";
open (fileHandler1, $ARGV[1]."/".$ARGV[2]."_".$filter.".tsv") || die "Cannot open ".$ARGV[2]."_".$filter.".tsv.\n";
while($l=<fileHandler1>){
	my @array = split /	/,$l;
	my $chr1 = $array[1];
	my $chr2 = $array[2];
	my $brk1 = $array[15];
	my $brk2 = $array[19];
	my $str1 = $array[11];
	my $str2 = $array[12];
	
	print fileWriter1 "chr".$chr1."\t".$brk1."\tchr".$chr2."\t".$brk2."\tAVG\t".$str1."\t".$str2."\n";

}
close(fileHandler1);
close(fileWriter1);

