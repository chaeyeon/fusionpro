#!/bin/bash

cd software

tar -zxvf SOAPfuse-v1.27.tar.gz
tar -zxvf tophat-2.0.14.Linux_x86_64.tar.gz

unzip bowtie-1.1.1-linux-x86_64.zip
unzip MapSplice-v2.1.8.zip

cd MapSplice-v2.1.8
make 

cd ..
tar -zxvf cufflinks-2.2.1.Linux_x86_64.tar.gz

unzip oncofuse-1.1.1.zip

