#!/bin/bash

perl -I ${PWD}/../../bin ${PWD}/../../bin/FusionPro-C.pl -bin ${PWD}/../../bin -filter IP -cdna ${PWD}/cdna_for_custom.fasta -gtf ${PWD}/exon_coord_for_custom.tsv -i ${PWD}/test_input_for_custom.tsv -o ${PWD} -r ${PWD}/../../library
