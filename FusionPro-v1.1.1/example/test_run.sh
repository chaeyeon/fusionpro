#!/bin/bash
wget ftp://ftp.cbcb.umd.edu/pub/data/tophat-fusion/SRR064287.tar.gz
tar -zxvf SRR064287.tar.gz
python ${PWD}/../bin/FusionPro.py -m RunPipeline -c ${PWD}/config_test.txt -f IPX -p 8 -qs phred33 -o ${PWD}/output_KPL4_test -1 ${PWD}/SRR064287/SRR064287_1.fastq -2 ${PWD}/SRR064287/SRR064287_2.fastq
