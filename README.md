## FusionPro

FusionPro is modular proteogenomics pipeline for identifying fusion proteoforms. 
Our pipeline provides sensitive gene fusion caller, which is composed of selective workflow of SOAPfuse, TopHat-fusion and MapSplice2, to predict fusion genes in RNA-Seq data. 
By using filtration module (IPX), this pipeline also filters out possible false predictions and false splicing isoforms to make sequences of putative fusion transcripts. 
Finally, it builds customized database by 3-frame translation from predicted fusion transcripts or a User-specified input of fusion events for identifying fusion-derive peptides in MS/MS data. 

---

## Requirements

**System requirements**  

1. at least 8Gb RAM, linux 64bit
2. perl & python (2.4.3 or higher)
3. g++ complier (4.3.3 =>, =< 4.8)
4. zlib and ncurses 
5. wget
6. java (Optional for Oncofuse)

**Perl Library** - packaged in /bin folder  

1. LWP::Simple.pm
2. Tie::IxHash.pm

---

## Installation

Download of FusionPro is available at https://bitbucket.org/chaeyeon/fusionpro/downloads/.

1. Extract all source files of FusionPro  

2. Install by extracting component programs  
	```
	cd /path/to/FusioPro-vX.X.X  
	```

	```
	sh install_software.sh  
	```

3. Export path for perl modules in SOAPfuse, bowtie and samtools.  
~~~
	export PERL5LIB=$PERL5LIB:/path/to/FusionPro/software/SOAPfuse-v1.27/source/bin/perl_module  
	export PATH=$PATH:/path/to/FusionPro/software/SOAPfuse-v1.27/source/bin/aln_bin/samtools-0.1.18  
	export PATH=$PATH:/path/to/FusionPro/software/bowtie-1.1.1 
~~~

---

## Preparation

First, data and source directories in /config/**config.txt** must be set by the user.

Downloading references would be automatically done with **CreateReference** module.  
(Output directory is for log files and the path should be specified as an absolute path.)
~~~
	python /path/to/FusionPro/bin/FusionPro.py -c /path/to/FusionPro/config/config.txt -m CreateReference -o /path/to/FusionPro/reference_log/
~~~

You can also download prebuilt reference sets (GRCh38.82) of FusionPro at https://goo.gl/RfaKJq.

Depending on the individual RNA-Seq data, the following parameters in /config/**config.txt** must be set by the user.

1. Read length
2. Enzyme (e.g. trypsin, chymotrypsin, Asp-N)
3. Ensembl version & Ensembl genome version
4. UCSC genome version
5. Abundance cutoff (Cufflinks FPKM)
6. "Source directory" & "SOAPfuse directory" to be an absolute path
7. Two Tophat-fusion options (mate_inner_dist, mate_std_dev)
8. Postfix of RNA-seq file (param name= "PA_all_fq_postfix", used for SOAPfuse)
9. SOAPfuse cpu process (param name= "PA_all_process_of_align_software", used for SOAPfuse)

---

## Run FusionPro

**Usage**
~~~
use "python FusionPro.py --help" or "python FusionPro.py -h" for more information.  
Run FusionPro for fusion proteoform discovery.  
~~~

**Optional Arguments:**  
~~~
	-h, --help			show this help message and exit  
	-p P				specify number of processor to use (default: 1)  
	-c C				specify config file path  
	-m M				specify module to run (default: RunPipeline)  
							- CreateReference  
							- RunPipeline (RunSoapfuse, RunTophatfusionS, RunMapspliceS, RunCufflinks, ProcessResults, FindFusionProteoform)
							- RunSoapfuse  
							- RunTophatfusion or RunTophatfusionS  
							- RunMapsplice or RunMapspliceS  
							- RunCufflinks 
							- ProcessResults  
							- FindFusionProteoform  
							- MergeMultipleResults  
	-qs QS				specify quality-scale  
							- Illumina 1.8+, Sanger		-> phred33  
							- Illumina 1.3+ ~ 1.7+		-> phred64  
							- Solexa64			-> solex64  
							(default: phred33)  
	-f F				choose filtering options  
							- Intron filtration					-> I  
							- Paralog filtration					-> P  
							- Expression filtration (Cufflinks)			-> X  
							- No filtration						-> N  
							(default: IPX)  
	-o O				specify output directory absolute path  
	-1 FQ, --fq FQ		specify fastq file 1 path  
 	-2 FQ2, --fq2 FQ2	specify fastq file 2 path  
~~~
---

## Test Running of FusionPro

	1. Move to the example directory in the FusionPro path
	2. Specify FusionPro paths in config_test.txt
	3. Run the command to automatically test FusionPro with KPL4 cell line RNA-Seq data. (Edgren et al.)
	   >sh test_run.sh

---

## Run FusionPro-C
From user-specified fusion transcripts to make Customized DB.

**Usage** (All path should be absolute path)
~~~
	export PERL5LIB=$PERL5LIB:/path/to/FusionPro-vX.X.X/bin
	perl /path/to/FusionPro/bin/FusionPro-C.pl -filter IPX -cdna /path/to/cdna.fasta -gtf /path/to/exon_coord.tsv -i gene_fusion.tsv -o /path/to/output -r /path/to/FusionPro/library  
	[-fa_dir path/to/genome_fa -abun path/to/abundance.txt -cutoff 0]
~~~

**Custom Reference:**  

	1. cDNA fasta: Ensembl cDNA fasta or custom cDNA fasta with transcript_id (as used in gtf) in its accession
	2. gtf (Ensemble gtf or custom gtf)
	   : <gene_name> <chr> <exon_start_list> <exon_end_list> <transcript_id> <transcript_start> <transcript_end> <5'UTR_start> <5'UTR_end> <3'UTR_start> <3'UTR_end>
	3. genome fasta (necessary when user do not filter out intronic junctions)
	   : Each file name and assession have to be same chromosome format as used in gtf (e.g. either 'chr1[.fa]' or '1[.fa]')
	4. Abundance: <transcript_id> <abundance>


**Test Running of FusionPro-C**

	1. Move to the example/FusionPro-C directory in the FusionPro path
	2. Run below command to automatically test FusionPro-C.
	   >sh test_run_custom.sh


---

## Output (in the directory /combined_result)

* **FusionPro-merged_result_<filter_option>.tsv**
```
	1. <Sample_id>
	2. <5'chromosome>
	3. <3'chromosome>
	4. <5'gene>
	5. <5'transcript_id>
	6. <5'transcript_start(+),end(-)>
	7. <5'transcript_end(+),start(-)>
	8. <3'gene>
	9. <3'transcript_id>
	10. <3'transcript_start(+),end(-)>
	11. <3'transcript_end(+),start(-)>
	12. <5'strand>
	13. <3'strand>
	14. <5'junction_region>
	15. <5'junction_region_start(+),end(-)>
	16. <5'junction_coordinate>
	17. <5'junction_region_end(+),start(-)>
	18. <3'junction_region>
	19. <3'junction_region_start(+),end(-)>
	20. <3'junction_coordinate>
	21. <3'junction_region_end(+),start(-)>
	22. <5'junction_location>
	23. <3'junction_location>
	24. <Fusion_junction_splicing>
	25. <MitelmanDB_annotation>
	26. <MitelmanDB_URL>
	27. <Spanning_read>
	28. <Encompassing_pair>
	29. <Software>
	30. <Translation>
```
* **FusionPro-final_fusion_cDNA_<filter_option>.fasta**  
  cDNA sequence of fusion transcript 

* **FusioPro-final_fusion_proteoform_<filter_option>.fasta**  
  Full-length fusion proteoform sequences (so-called 'Customized DB') for identifying all fusion-derived peptides 

* **FusionPro-final_fusion_tryptic_peptide_<filter_option>.fasta**  
  Only the sequences of fusion junction spanning tryptic peptides (2 missed cleaveages are allowed) for identifying fusion protein 

* **FusionPro-fusion_transcript_prediction_summary_<filter_option>.html**  
  Venn diagram of prediction results of each SOAPfuse, TopHatfusionS, MapSpliceS run

* **FusionPro-driver_prediction_<filter_option>.txt**  
  Predicted driver fusion gene (driver_prob > 0.5) obtained from Oncofuse (oncofuse_coord.txt -> oncofuse_out.txt)  
```<junction_location> <5'gene> <3'gene> <p_value_correlation> <driver_probability>```

---

* **[Warning] FusionPro's Cufflinks workflow uses only reads supporting fusion events.  
Thus, this FPKM output is for estimation of fusion transcript abundance not for general quantification.**

---